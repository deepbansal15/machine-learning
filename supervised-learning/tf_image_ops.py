import os
import tensorflow as tf

IMAGE_SIZE = 224
CROP_PADDING = 32

def square_centre_crop(image_bytes, image_size=IMAGE_SIZE):
    image_shape = tf.io.extract_jpeg_shape(image_bytes)
    image_height = image_shape[0]
    image_width = image_shape[1]
    padded_center_crop_size = tf.cast(
        ((image_size / (image_size + CROP_PADDING)) *
        tf.cast(tf.minimum(image_height, image_width), tf.float32)),
        tf.int32)
    
    offset_height = (image_height - padded_center_crop_size + 1) // 2
    offset_width = (image_width - padded_center_crop_size + 1) // 2

    crop_window = tf.stack([offset_height, offset_width, 
                            padded_center_crop_size, padded_center_crop_size])
    
    image = tf.io.decode_and_crop_jpeg(image_bytes, crop_window, channels=3)
    image = tf.image.resize_images(image, size=[IMAGE_SIZE, IMAGE_SIZE], 
                            method=tf.image.ResizeMethod.BICUBIC)
    return image

def main():

    session = tf.InteractiveSession()

    images_path = os.path.join('/home/deepbansal/machine-learning/sample_images/','*.JPEG') 
    images_glob = tf.io.gfile.glob(images_path)
    for image in images_glob:
        print(image)
        with tf.io.gfile.GFile(image,'rb') as f:
            image_data = f.read()
            cropped_image = square_centre_crop(image_data)
            cropped_image.eval()

    session.close()
    pass

if __name__ == '__main__':
    main()