import tensorflow as tf
import tensorflow_probability as tfp

import os
import math
import random

from absl import app
from absl import flags

from google.cloud import storage

tfd = tfp.distributions
FLAGS = flags.FLAGS

# Imagenet input size 500x375
# All the images are jpeg coded

IMAGE_SIZE = 224
CROP_PADDING = 32

def decode_square_centre_crop(image_bytes, image_size=IMAGE_SIZE):
    image_shape = tf.io.extract_jpeg_shape(image_bytes)
    image_height = image_shape[0]
    image_width = image_shape[1]
    padded_center_crop_size = tf.cast(
        ((image_size / (image_size + CROP_PADDING)) *
        tf.cast(tf.minimum(image_height, image_width), tf.float32)),
        tf.int32)
    
    offset_height = (image_height - padded_center_crop_size + 1) // 2
    offset_width = (image_width - padded_center_crop_size + 1) // 2

    crop_window = tf.stack([offset_height, offset_width, 
                            padded_center_crop_size, padded_center_crop_size])
    
    image = tf.io.decode_and_crop_jpeg(image_bytes, crop_window, channels=3)
    image = tf.image.resize_images(image, size=[IMAGE_SIZE, IMAGE_SIZE], 
                            method=tf.image.ResizeMethod.BICUBIC)
    return image

def flip_image(image):
    # Why only horizontal??
    # TODO Try only with vertical flip and random choice between both
    return tf.image.random_flip_left_right(image)

def preprocess_image(image_bytes, use_bfloat16=False, image_size=IMAGE_SIZE):
    image = decode_square_centre_crop(image_bytes)
    image = flip_image(image)
    image = tf.reshape(image, [image_size, image_size, 3])
    return image

# Takes care of extracting and transforming the data

def input_fn():

    pass

def main(argv):
    del argv
    tf.logging.set_verbosity(tf.logging.INFO)
    tf.logging.info('hello resnet')
    
if __name__ == '__main__':
    app.run(main)