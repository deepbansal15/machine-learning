import os
import tarfile
import random
import time
import math

import tensorflow as tf
import numpy as np

from google.cloud import storage

'''
the parameters for randomly choosing lighting and trajectories remains
the same. We selected two layouts from each type (bathroom, kitchen, office, living room, and bedroom) for the
validation and test sets making the layout split
'''

TRAINING_DIRECTORY = 'train'
SCENENET_DATA_DIR = 'scenenet/0'
NYU_DATA_DIR = 'nyuv2_data'
GCS_BUCKET = 'tpu-gke-bucket'

SCENENET_IMAGES_COUNT = 50000
NYU_IMAGE_COUNT = 1449

WIDTH = 320
HEIGHT = 240

TRAINING_SHARDS = 64


# Create TFRecord and upload to dataset

def check_and_create_dir(directory):
    if not tf.gfile.Exists(directory):
        tf.gfile.MakeDirs(directory)

def int64_list_feature(value):
    if not isinstance(value,list):
        value = [value]
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))

def bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def convert_to_example(filename, image_buffer, depth_buffer, height, width):
    example = tf.train.Example(features=tf.train.Features(feature={
        'image/height': int64_list_feature(height),
        'image/width': int64_list_feature(width),
        'image/encoded': bytes_feature(image_buffer),
        'depth/encoded': bytes_feature(depth_buffer)
    }))
      
    return example

class ImageCoder(object):
  def __init__(self):
    self._sess = tf.Session()
    # Initializes function that converts PNG to JPEG data.

    self._decode_png_data = tf.placeholder(dtype=tf.string)
    self._decode_png = tf.image.decode_png(self._decode_png_data, dtype=tf.uint16)

    self._decode_jpeg_data = tf.placeholder(dtype=tf.string)
    self._decode_jpeg = tf.image.decode_jpeg(self._decode_jpeg_data, channels=3)

  def decode_png(self, image_data):
    image = self._sess.run(self._decode_png,
                          feed_dict={self._decode_png_data: image_data})
    print(image.shape)
    return image

  def decode_jpeg(self, image_data):
    image = self._sess.run(self._decode_jpeg,
                           feed_dict={self._decode_jpeg_data: image_data})
    print(image.shape)
    assert len(image.shape) == 3
    assert image.shape[2] == 3
    return image

def process_image(filename, coder):
    # Read the image file.
    with tf.gfile.FastGFile(filename, 'rb') as f:
        image_data = f.read()

    if 'png' in filename:
        decoded_image = coder.decode_png(image_data)
    else:
        coder.decode_jpeg(image_data)
    
    # Fixed height and width
    return image_data, HEIGHT, WIDTH

def process_images_in_shard(coder, output_file, filenames):
    writer = tf.io.TFRecordWriter(path=output_file)

    for file in filenames:
        rgb_image_data, _, _ = process_image(file[0],coder)
        depth_image_data, height, width = process_image(file[1],coder)
        example = convert_to_example(file, rgb_image_data, depth_image_data,height,width)
        writer.write(example.SerializeToString())
    
    writer.close()

def process_dataset(filenames, output_directory, prefix):
    check_and_create_dir(output_directory)
    coder = ImageCoder()
    chunk_size = int(math.ceil(len(filenames) / TRAINING_SHARDS))
    files = []

    for shard in range(TRAINING_SHARDS):
        chunk_files = filenames[shard * chunk_size : (shard + 1) * chunk_size]

        if len(chunk_files) < chunk_size:
            print(
                'shard index : {0}, chunk size : {1}'.format(shard,len(chunk_files)))
        
        # TFRecord file
        output_file = os.path.join(
            output_directory, '%s-%.5d-of-%.5d' % (prefix, shard, TRAINING_SHARDS))
        process_images_in_shard(coder,output_file, chunk_files)
        print('Finished writing file: %s' % output_file)
        files.append(output_file)

    return files

def get_training_files(scratch_dir, limit_scenet_images=False):
    # Shuffle training records to ensure we are distributing classes
    # across the batches.
    random.seed(0)
    def make_shuffle_idx(n, elements):
        order = [i for i in range(n)]
        random.shuffle(order)
        order = random.sample(order, elements)
        return order
    
    # Glob all the training files
    scenenet_rgb_training_files = tf.gfile.Glob(
        os.path.join(scratch_dir, SCENENET_DATA_DIR, '*', 'photo', '*.jpg'))

    scenenet_training_files = []
    for file in scenenet_rgb_training_files:
        depth_file = file.replace('photo','depth').replace('jpg','png')
        scenenet_training_files.append((file,depth_file))
    
    max_scenenet_images = len(scenenet_training_files)
    if limit_scenet_images:
        max_scenenet_images = SCENENET_IMAGES_COUNT
    
    shuffle_idx = make_shuffle_idx(len(scenenet_training_files), max_scenenet_images)
    scenenet_training_files = [scenenet_training_files[i] for i in shuffle_idx]

    nyu_rgb_training_files = tf.gfile.Glob(
        os.path.join(scratch_dir, NYU_DATA_DIR, 'images', '*.jpg'))

    nyu_training_files = []
    for file in nyu_rgb_training_files:
        depth_file = file.replace('images','depth').replace('jpg','png')
        nyu_training_files.append((file,depth_file))
    
    shuffle_idx = make_shuffle_idx(len(nyu_training_files), len(nyu_training_files))
    nyu_training_files = [nyu_training_files[i] for i in shuffle_idx]

    training_files = scenenet_training_files[:] + nyu_training_files[:]
    shuffle_idx = make_shuffle_idx(len(training_files), len(training_files))
    training_files = [training_files[i] for i in shuffle_idx]
    return training_files

def nyu_training_files(scratch_dir):
    # Shuffle training records to ensure we are distributing classes
    # across the batches.
    random.seed(0)
    def make_shuffle_idx(n, elements):
        order = [i for i in range(n)]
        random.shuffle(order)
        order = random.sample(order, elements)
        return order
    
    nyu_rgb_training_files = tf.gfile.Glob(
        os.path.join(scratch_dir, NYU_DATA_DIR, 'images', '*.jpg'))

    nyu_training_files = []
    for file in nyu_rgb_training_files:
        depth_file = file.replace('images','depth').replace('jpg','png')
        nyu_training_files.append((file,depth_file))
    
    shuffle_idx = make_shuffle_idx(len(nyu_training_files), len(nyu_training_files))
    nyu_training_files = [nyu_training_files[i] for i in shuffle_idx]

    training_files = nyu_training_files[:]
    shuffle_idx = make_shuffle_idx(len(training_files), len(training_files))
    training_files = [training_files[i] for i in shuffle_idx]
    return training_files

def create_records(scratch_dir):
    training_files = get_training_files(scratch_dir, True)
    print(len(training_files))

    training_records = process_dataset(
        training_files,
        os.path.join(scratch_dir, TRAINING_DIRECTORY), 
        TRAINING_DIRECTORY)

def create_nyu_records(scratch_dir):
    training_files = nyu_training_files(scratch_dir)
    print(len(training_files))

    training_records = process_dataset(
        training_files,
        os.path.join(scratch_dir, TRAINING_DIRECTORY), 
        TRAINING_DIRECTORY)
    return training_records

def upload_nyu_gcs(training_records):
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(GCS_BUCKET)

    def upload_files(records, gcs_blob_path):
        for record_path in records:
            record_name = os.path.basename(record_path)
            destination_blob_name = os.path.join(gcs_blob_path, record_name)

            blob = bucket.blob(destination_blob_name)
            blob.upload_from_filename(record_path)

            print('Finished uploading file: %s' % record_name)
    
    train_cloud_path = os.path.join('nyuv2', TRAINING_DIRECTORY)
    upload_files(training_records, train_cloud_path)

def upload_gcs(scratch_dir):
    training_records = tf.gfile.Glob(
        os.path.join(scratch_dir, TRAINING_DIRECTORY, '*'))
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(GCS_BUCKET)

    def upload_files(records, gcs_blob_path):
        for record_path in records:
            record_name = os.path.basename(record_path)
            destination_blob_name = os.path.join(gcs_blob_path, record_name)

            blob = bucket.blob(destination_blob_name)
            blob.upload_from_filename(record_path)

            print('Finished uploading file: %s' % record_name)
    
    train_cloud_path = os.path.join('nyu_scenenet', TRAINING_DIRECTORY)
    upload_files(training_records, train_cloud_path)

def get_max_min_depth(scratch_dir):
    training_files = get_training_files(scratch_dir, False)
    coder = ImageCoder()

    max_depths = []
    min_depths = []
    index = 0
    with tf.Session() as sess:
        for file in training_files:
            with tf.gfile.FastGFile(file[1], 'rb') as f:
                image_data = f.read()
                decoded_image = coder.decode_png(image_data)
                # Calculating max depth for normalization
                max_depth_val = tf.reduce_max(decoded_image)
                min_depth_val = tf.reduce_min(decoded_image)
                max_depth, min_depth = sess.run([max_depth_val, min_depth_val])
                max_depths.append(max_depth)
                min_depths.append(min_depth)
                del(image_data)
                index += 1
                print('max_depth {}, index {}'.format(max_depth,index))
    
    print('max depth {}, min depth {}'.format(max(max_depths), min(min_depths)))

# create_records('/home/deepbansal/machine-learning/supervised-learning/src/scenenet_nyu_dataset')
# upload_gcs('/home/deepbansal/machine-learning/supervised-learning/src/scenenet_nyu_dataset')
#get_max_min_depth('/home/deepbansal/nyu_scenenet')

training_records = create_nyu_records('/home/deepbansal/nyu_scenenet')
upload_nyu_gcs(training_records)

# Assuming that above code ran once

# Load the dataset for imput function
