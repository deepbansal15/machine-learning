
# Cross replica mean and moments

import tensorflow as tf
from tensorflow.contrib.tpu.python.tpu import tpu_function

def cross_replica_mean(inputs):
    num_cores = tpu_function.get_tpu_context().number_of_shards
    return tf.tpu.cross_replica_sum(inputs) /  tf.cast(num_cores, inputs.dtype)

def cross_replica_moments(inputs, axis, parallel=True):
    # Calculate mean
    # Local mean across the desired axis
    mean = tf.math.reduce_mean(inputs,axis=axis)
    mean = cross_replica_mean(mean)
    if parallel:
        # E[x^2] - (E[x])^2
        mean_of_squared = tf.math.reduce_mean(tf.square(inputs),axis=axis)
        mean_of_squared = cross_replica_mean(mean_of_squared)

        mean_squared = tf.square(mean)
        variance = mean_of_squared - mean_squared
    else:
        # E[(x-E[x])^2]
        variance = tf.math.reduce_mean(tf.square(inputs - mean),axis=axis)
    
    variance = cross_replica_mean(variance)
    return mean, variance
        

