import os
import tensorflow as tf

from absl import app
from absl import flags

import resnet_50

flags.DEFINE_string('gcp_project_id', 'reinforcement-learning-244916', 'GCP project id')
flags.DEFINE_string('data_dir', 'gs://tpu-gke-bucket/Imagenet', 'Imagenet input directory')
flags.DEFINE_string('model_dir', 'gs://tpu-gke-bucket/Imagenet/resnet/resnet-200-mvAvg', 'Model output directory')
flags.DEFINE_string('tpu_name', 'tpu-vm-v3-1', 'TPU grpc address')
# Not sure if its per shard/TPU core (Its for whole TPU)
flags.DEFINE_integer('batch_size', 512, 'batch_size for train and evaluation')
flags.DEFINE_integer('num_total_steps', 130000, 'Epoch to train for')
flags.DEFINE_integer('num_shards', 8, 'Number of TPU cores to use')
flags.DEFINE_integer('checkpoint_steps', 5000, 'Number of checkpoint between each evaluation')
flags.DEFINE_string('mode', 'train_and_eval', '3 modes supported Train, Train/Eval, Eval')

flags.DEFINE_integer('resnet_depth', 200, 'Resnet depth')

FLAGS = flags.FLAGS

# Dataset details
TOTAL_NUM_IMAGES = 1281167
TOTAL_EVAL_IMAGES = 50000
# 1 extra for Error
NUM_CLASSES = 1001
IMAGE_SIZE = 224
CROP_PADDING = 32

# Fetching buffers constants
PREFETCH_BUFFER_SIZE = 8 * 1024 * 1024
NUM_FILES_INFEED = 8
SHUFFLE_BUFFER_SIZE = 1000

# Training constant
MOMENTUM = 0.9
WEIGHT_DECAY = 1e-4
BASE_LR = 0.1
LR_SCHEDULE = [  # (LR multiplier, epoch to start)
    (1.0 / 6, 0), (2.0 / 6, 1), (3.0 / 6, 2), (4.0 / 6, 3), (5.0 / 6, 4),
    (1.0, 5), (0.1, 30), (0.01, 60), (0.001, 80), (0.0001, 90)
]

MEAN_RGB = [0.485 * 255, 0.456 * 255, 0.406 * 255]
STDDEV_RGB = [0.229 * 255, 0.224 * 255, 0.225 * 255]

def learning_rate_schedule(current_epoch):
  """Handles linear scaling rule, gradual warmup, and LR decay."""
  scaled_lr = BASE_LR * (FLAGS.batch_size / 256.0)

  decay_rate = scaled_lr
  for mult, start_epoch in LR_SCHEDULE:
    decay_rate = tf.where(current_epoch < start_epoch, decay_rate,
                          scaled_lr * mult)

  return decay_rate

def decode_square_centre_crop(image_bytes, image_size=IMAGE_SIZE):
    image_shape = tf.io.extract_jpeg_shape(image_bytes)
    image_height = image_shape[0]
    image_width = image_shape[1]
    padded_center_crop_size = tf.cast(
        ((image_size / (image_size + CROP_PADDING)) *
        tf.cast(tf.minimum(image_height, image_width), tf.float32)),
        tf.int32)
    
    offset_height = (image_height - padded_center_crop_size + 1) // 2
    offset_width = (image_width - padded_center_crop_size + 1) // 2

    crop_window = tf.stack([offset_height, offset_width, 
                            padded_center_crop_size, padded_center_crop_size])
    
    image = tf.io.decode_and_crop_jpeg(image_bytes, crop_window, channels=3)
    image = tf.image.resize_images(image, size=[IMAGE_SIZE, IMAGE_SIZE], 
                            method=tf.image.ResizeMethod.BICUBIC)
    return image

def flip_image(image):
    # Why only horizontal??
    # TODO Try only with vertical flip and random choice between both
    return tf.image.random_flip_left_right(image)

def preprocess_image(image_bytes, use_bfloat16=False, image_size=IMAGE_SIZE):
    image = decode_square_centre_crop(image_bytes)
    image = flip_image(image)
    image = tf.reshape(image, [image_size, image_size, 3])
    image = tf.image.convert_image_dtype(
        image, dtype=tf.bfloat16 if use_bfloat16 else tf.float32)
    return image

class ImagenetInput:
    def __init__(self, is_training, data_dir=None):
        self.data_dir = data_dir
        self.is_training = is_training
    
    # Parse a single example protobuffer inside tfrecord and apply a preprocess function on it
    def dataset_parser(self, value):
        features = {'image/encoded': tf.FixedLenFeature((), tf.string, ''),
                    'image/class/label': tf.FixedLenFeature([], tf.int64, -1),
                    'image/channels': tf.FixedLenFeature([], tf.int64, 3)}
    
        parsed = tf.parse_single_example(value, features)

        # Feels like this is un-necessary need to check by directly
        # changing features from () => []
        image_bytes = tf.reshape(parsed['image/encoded'], shape=[])
        image = preprocess_image(image_bytes, use_bfloat16=False)
        # Can do one hot encoding here
        image_label = tf.cast(tf.reshape(parsed['image/class/label'],shape=[]), dtype=tf.int32)

        return image, tf.one_hot(image_label, NUM_CLASSES)

    def __call__(self,params):
        # Retrieves the batch size for the current shard. The # of shards is
        # computed according to the input pipeline deployment. See
        # `tf.contrib.tpu.RunConfig` for details.
        batch_size = params['batch_size']

        # Shuffle the filenames to ensure better randomization
        file_pattern = os.path.join(self.data_dir, 'train/train-*'
                            if self.is_training else 'validation/validation-*')
        dataset = tf.data.Dataset.list_files(file_pattern, shuffle=False)
        if self.is_training:
            dataset = dataset.shuffle(buffer_size=1024)  # 1024 files in dataset

        if self.is_training:
            dataset = dataset.repeat()

        def prefetch_dataset(filename):
            buffer_size = PREFETCH_BUFFER_SIZE
            dataset = tf.data.TFRecordDataset(filename, buffer_size=buffer_size)
            return dataset

        dataset = dataset.apply(
            tf.contrib.data.parallel_interleave(
                prefetch_dataset, cycle_length=NUM_FILES_INFEED, sloppy=True))
        dataset = dataset.shuffle(SHUFFLE_BUFFER_SIZE)

        dataset = dataset.map(self.dataset_parser, num_parallel_calls=128)
        dataset = dataset.prefetch(batch_size)
        dataset = dataset.batch(batch_size, drop_remainder=True)
        dataset = dataset.prefetch(2)  # Prefetch overlaps in-feed with training
        return dataset

def model_fn(features, labels, mode, params):
    # Normalize the images
    features -= tf.constant(MEAN_RGB, shape=[1, 1, 3], dtype=features.dtype)
    features /= tf.constant(STDDEV_RGB, shape=[1, 1, 3], dtype=features.dtype)

    # Get the model
    # logits from dense layer 
    # Note: Only using densenet-169 model for now
    def build_network():
        model = resnet_50.resnet_model(num_classes=NUM_CLASSES, depth=FLAGS.resnet_depth, use_tpu=True)
        return model(inputs=features, is_training=(mode == tf.estimator.ModeKeys.TRAIN))
    
    # with tf.contrib.tpu.bfloat16_scope():
    logits = build_network()
    
    logits = tf.cast(logits, tf.float32)

    # If predict mode
    if mode == tf.estimator.ModeKeys.PREDICT:
        predictions = {
            'classes': tf.argmax(logits, axis=1),
            'probabilities': tf.nn.softmax(logits, name='softmax_tensor')
        }
        return tf.estimator.EstimatorSpec(
            mode=mode,
            predictions=predictions,
            export_outputs={
                'classify': tf.estimator.export.PredictOutput(predictions)
            })

    # ------- Setup loss --------------
    loss = None
    # Cross entropy loss with sigmoid for normalizing
    cross_entropy_loss = tf.losses.softmax_cross_entropy(
        onehot_labels=labels,logits=logits)

    # Add weight decay to the loss. We exclude weight decay on the batch
    # normalization variables because it slightly improves accuracy.
    loss = cross_entropy_loss + WEIGHT_DECAY * tf.add_n([
      tf.nn.l2_loss(v)
      for v in tf.trainable_variables()
      if "batch_norm" not in v.name
    ])

    # ------- Setup train operation --------------
    # Calculate learning rate based upon the step
    global_step = tf.train.get_global_step()
    current_epoch = (
        tf.cast(global_step, tf.float32) / params["batches_per_epoch"])
    learning_rate = learning_rate_schedule(current_epoch)

    def host_call_fn(gs, loss, lr, ce):
        gs = gs[0]
        print('********* LOSS {} ***********'.format(loss[0]))
        with tf.contrib.summary.create_file_writer(
            FLAGS.model_dir,
            max_queue=params['iterations_per_loop']).as_default():
            with tf.contrib.summary.always_record_summaries():
                tf.contrib.summary.scalar('loss', loss[0], step=gs)
                tf.contrib.summary.scalar('learning_rate', lr[0], step=gs)
                tf.contrib.summary.scalar('current_epoch', ce[0], step=gs)
                return tf.contrib.summary.all_summary_ops()

    gs_t = tf.reshape(global_step, [1])
    loss_t = tf.reshape(loss, [1])
    lr_t = tf.reshape(learning_rate, [1])
    ce_t = tf.reshape(current_epoch, [1])

    host_call = (host_call_fn, [gs_t, loss_t, lr_t, ce_t])


    train_op = None
    if mode == tf.estimator.ModeKeys.TRAIN:
        # Optimizer setup
        local_optimizer = tf.train.MomentumOptimizer(
            learning_rate=learning_rate,momentum=MOMENTUM)
        distribute_optimizer = tf.contrib.tpu.CrossShardOptimizer(local_optimizer)

        # batch normalize required update_ops to be added as train_op
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            train_op = distribute_optimizer.minimize(loss, global_step)
    
    # ------- Setup evaluation metrics --------------
    # Can also seperate the metric function
    metrics = None
    if mode == tf.estimator.ModeKeys.EVAL:
        def metric_fn(labels, logits):

            predictions = tf.argmax(logits, axis=1)
            actual = tf.argmax(labels, axis=1)

            top_1_accuracy = tf.metrics.accuracy(labels=actual, predictions=predictions)
            in_top_5 = tf.cast(tf.nn.in_top_k(predictions=logits, targets=actual, k=5), 
                               tf.float32)
            top_5_accuracy = tf.metrics.mean(in_top_5)

            return {
                'top_1_accuracy': top_1_accuracy,
                'top_5_accuracy': top_5_accuracy,
            }
        
        metrics = (metric_fn,[labels, logits])
    
    return tf.estimator.tpu.TPUEstimatorSpec(
            mode=mode, loss=loss, train_op=train_op, 
            eval_metrics=metrics, host_call=host_call)


def main(argv):
    del(argv)
    mode = FLAGS.mode
    # tf.logging.set_verbosity(tf.logging.INFO)
    cluster_resolver = tf.distribute.cluster_resolver.TPUClusterResolver(
        tpu=FLAGS.tpu_name,
        project=FLAGS.gcp_project_id
    )

    iteration_per_loop = FLAGS.checkpoint_steps
    batches_per_epoch = TOTAL_NUM_IMAGES // FLAGS.batch_size
    eval_steps = TOTAL_EVAL_IMAGES // FLAGS.batch_size

    if mode is 'eval':
        iteration_per_loop = eval_steps

    # Retains last five checkpoints
    tpu_config = tf.estimator.tpu.RunConfig(
        cluster=cluster_resolver,
        model_dir=FLAGS.model_dir,
        save_checkpoints_steps=FLAGS.checkpoint_steps,
        log_step_count_steps=iteration_per_loop,
        tpu_config=tf.estimator.tpu.TPUConfig(
            iterations_per_loop=iteration_per_loop,
            num_shards=FLAGS.num_shards,
        )
    )

    params = {
      'batches_per_epoch': batches_per_epoch,
      'iterations_per_loop': iteration_per_loop
    }

    estimator = tf.estimator.tpu.TPUEstimator(
        model_fn=model_fn,
        model_dir=FLAGS.model_dir,
        config=tpu_config,
        params=params,
        train_batch_size=FLAGS.batch_size,
        eval_batch_size=FLAGS.batch_size,
    )

    if mode == 'train':
        tf.logging.info('Training for %d steps (%.2f epochs in total).' %
                    (FLAGS.num_total_steps, FLAGS.num_total_steps / batches_per_epoch))
        estimator.train(
            input_fn=ImagenetInput(True, FLAGS.data_dir),
            max_steps=FLAGS.num_total_steps
        )
    elif mode == 'train_and_eval':
        tf.logging.info('Training and evaluating for %d steps (%.2f epochs in total).' %
            (FLAGS.num_total_steps, FLAGS.num_total_steps / batches_per_epoch))
        current_step=0
        while current_step < FLAGS.num_total_steps:
            next_checkpoint = min(current_step + FLAGS.checkpoint_steps,
                                   FLAGS.num_total_steps 
                                )
            num_steps = next_checkpoint - current_step
            current_step = next_checkpoint
            estimator.train(
                input_fn=ImagenetInput(True, FLAGS.data_dir),
                steps=num_steps
            )

            tf.logging.info('Evaluating after epoch : %.2f' %
                (next_checkpoint // batches_per_epoch))

            eval_result = estimator.evaluate(
                input_fn=ImagenetInput(False,FLAGS.data_dir),
                steps=eval_steps
            )
            tf.logging.info('Eval results: %s' % eval_result)

    else:
        tf.logging.info('Evaluating with latest saved checkpoint')
        eval_result = estimator.evaluate(
            input_fn=ImagenetInput(False,FLAGS.data_dir),
            steps=eval_steps
        )
        tf.logging.info('Eval results: %s' % eval_result)
    

if __name__ == '__main__':
    app.run(main)