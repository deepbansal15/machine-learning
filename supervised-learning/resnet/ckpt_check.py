import tensorflow as tf


from absl import flags
work_dir = 'gs://tpu-gke-bucket/Imagenet/resnet/resnet-200-default'

FLAGS = flags.FLAGS

variables = tf.train.list_variables(tf.train.latest_checkpoint(work_dir))
print(*variables, sep='\n')