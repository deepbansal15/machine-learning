import tensorflow as tf
import numpy as np
import functools

import tf_ops

conv1x1 = functools.partial(tf_ops.conv2d, kh=1, kw=1, use_sn=False, use_bias=False)
conv3x3 = functools.partial(tf_ops.conv2d, kh=3, kw=3, use_sn=False, use_bias=False)
batch_norm = functools.partial(tf_ops.batch_norm, use_moving_averages=True,
                               scale=True,
                               offset=True)

def resnet_block(inputs, out_channels, 
                 use_projection,
                 strides, scope, 
                 is_training,
                 use_tpu):
    with tf.variable_scope(scope, values=[inputs]):
        stride_h = strides
        stride_w = strides
        shortcut = inputs
        
        if use_projection:
            shortcut = conv1x1(inputs=inputs, kn=out_channels,
                               stride_h=stride_h, stride_w=stride_w,
                               scope='shortcut_conv2d')
            shortcut = batch_norm(inputs=shortcut, 
                                  scope='shortcut_batch_norm', 
                                  is_training=is_training,
                                  use_tpu_cross_replica_mean=use_tpu)
            shortcut = tf.nn.relu(shortcut)
        
       
        y = conv3x3(inputs=inputs, kn=out_channels,
                    stride_h=stride_h, stride_w=stride_w,
                    scope='conv2d_1')
        y = batch_norm(inputs=y, 
                       scope='batch_norm_1', 
                       is_training=is_training,
                       use_tpu_cross_replica_mean=use_tpu)
        y = tf.nn.relu(y)

        y = conv3x3(inputs=y, kn=out_channels,
                    stride_h=1, stride_w=1,
                    scope='conv2d_2')
        y = batch_norm(inputs=y, 
                       scope='batch_norm_2', 
                       is_training=is_training,
                       use_tpu_cross_replica_mean=use_tpu)
        y = tf.nn.relu(y)
        
        print(shortcut.shape)
        print(y.shape)
        return tf.nn.relu(y + shortcut)


def block_group(inputs, block_count, 
                out_channels,
                strides,
                is_training, scope, 
                use_tpu):
    
    block = functools.partial(
                resnet_block, 
                out_channels=out_channels,
                is_training=is_training,
                use_tpu=use_tpu)   
    
    with tf.variable_scope(scope, values=[inputs]):

        # Only first block uses projection and strides
        x = block(inputs=inputs,
                  strides=strides,
                  use_projection=True,
                  scope='resnetblock_1')
    
        for i in range(1, block_count):
            x = block(inputs=x,
                      strides=1,
                      use_projection=False,
                      scope='resnetblock_{}'.format(i+1))
    
        return x

def resnet_generator(layers, num_classes,use_tpu):
    
    def model(inputs, is_training):
        # Input block
        inputs = tf_ops.conv2d(
            inputs=inputs, kh=7, 
            kw=7, kn=64,
            stride_h=2, stride_w=2,
            scope='initial_conv',
            use_bias=False,
            use_sn=False)
        inputs = batch_norm(inputs=inputs, 
                           scope='initial_batch_norm', 
                           is_training=is_training,
                           use_tpu_cross_replica_mean=use_tpu)
        inputs = tf.nn.relu(inputs)

        inputs = tf.nn.max_pool2d(input=inputs, ksize=[1,3,3,1], 
                                  strides=[1,2,2,1], padding='SAME',
                                  name='initial_maxpool')

        # Middle blocks
        inputs = block_group(
            inputs=inputs, out_channels=64, block_count=layers[0],
            strides=1, is_training=is_training, scope='block_group1', 
            use_tpu=use_tpu)
        inputs = block_group(
            inputs=inputs, out_channels=128, block_count=layers[1],
            strides=2, is_training=is_training, scope='block_group2',
            use_tpu=use_tpu)
        inputs = block_group(
            inputs=inputs, out_channels=256, block_count=layers[2],
            strides=2, is_training=is_training, scope='block_group3',
            use_tpu=use_tpu)
        inputs = block_group(
            inputs=inputs, out_channels=512, block_count=layers[3],
            strides=2, is_training=is_training, scope='block_group4',
            use_tpu=use_tpu)


        # Output block
        inputs = tf.math.reduce_mean(input_tensor=inputs, axis=(1,2), keepdims=True)
        inputs = tf.reshape(inputs, [-1, 512])
        output = tf_ops.linear_fc(inputs=inputs, output_size=num_classes, scope='final_dense')

        return output
    
    return model

def resnet_model(num_classes, depth,use_tpu):
    model_params = {
        18: {'layers': [2, 2, 2, 2]},
        34: {'layers': [3, 4, 6, 3]},
        50: {'layers': [3, 4, 6, 3]},
        101: {'layers': [3, 4, 23, 3]},
        152: {'layers': [3, 8, 36, 3]},
        200: {'layers': [3, 24, 36, 3]}
    }

    model_param = model_params[depth]
    return resnet_generator(layers=model_param['layers'], 
                     num_classes=num_classes, use_tpu=use_tpu)

# Test script

# if __name__ == '__main__':
#     inputs = np.ndarray(shape=(8,224,224,3), dtype=float)
#     inputs = tf.convert_to_tensor(inputs, dtype= tf.float32)

#     with tf.tpu.bfloat16_scope():
#         model = resnet_model(depth=50, 
#                      num_classes=1001, use_tpu=False)

#         logits = model(inputs=inputs, is_training=True)
    
#     logits = tf.cast(logits, tf.float32)
#     optimizer = tf.train.AdamOptimizer()

#     image_label = np.ndarray(shape=(8,1), dtype=int)
#     image_label = tf.convert_to_tensor(image_label, dtype= tf.int32)
#     image_label = tf.one_hot(image_label, 1001)

#     cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=image_label )
#     loss = tf.reduce_sum(cross_entropy) * (1.0 / 8.0)
#     train_op = optimizer.minimize(loss)

#     variables = tf.trainable_variables()
#     print(*variables, sep = "\n")

#     global_variables = tf.global_variables()
#     print(*global_variables, sep = "\n")

    
