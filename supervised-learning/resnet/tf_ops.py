# Contains helper operations for deep learning
# conv2d, linear, deconv2d, initializers

import tensorflow as tf
from tensorflow.python.training import moving_averages

import functools
import gin
import tpu_ops

# Weight initializer
@gin.configurable('initializer',whitelist=['method'])
def initializer(method='orthogonal',stddev=0.02):
    if method == 'orthogonal':
        return tf.initializers.orthogonal()
    elif method == 'normal':
        return tf.initializers.random_normal(stddev)


# Normalization
def _moving_moments_for_inference(mean, variance, is_training, decay):
    """Use moving averages of moments during inference.
    Args:
        mean: Tensor of shape [num_channels] with the mean of the current batch.
        variance: Tensor of shape [num_channels] with the variance of the current
        batch.
        is_training: Boolean, wheather to construct ops for training or inference
        graph.
        decay: Decay rate to use for moving averages.
    Returns:
        Tuple of (mean, variance) to use. This can the same as the inputs.
    """
    # Create the moving average variables and add them to the appropriate
    # collections.
    variable_collections = [
        tf.GraphKeys.MOVING_AVERAGE_VARIABLES,
        tf.GraphKeys.MODEL_VARIABLES, tf.GraphKeys.GLOBAL_VARIABLES,
    ]
    # Disable partition setting for moving_mean and moving_variance
    # as assign_moving_average op below doesn"t support partitioned variable.
    moving_mean = tf.get_variable(
        "moving_mean",
        shape=mean.shape,
        initializer=tf.zeros_initializer(),
        trainable=False,
        partitioner=None,
        collections=variable_collections)
    
    moving_variance = tf.get_variable(
        "moving_variance",
        shape=variance.shape,
        initializer=tf.ones_initializer(),
        trainable=False,
        partitioner=None,
        collections=variable_collections)
    
    if is_training:
        # Update variables for mean and variance during training.
        update_moving_mean = moving_averages.assign_moving_average(
            moving_mean,
            tf.cast(mean, moving_mean.dtype),
            decay,
            zero_debias=False)
        
        update_moving_variance = moving_averages.assign_moving_average(
            moving_variance,
            tf.cast(variance, moving_variance.dtype),
            decay,
            zero_debias=False)
        tf.add_to_collection(tf.GraphKeys.UPDATE_OPS, update_moving_mean)
        tf.add_to_collection(tf.GraphKeys.UPDATE_OPS, update_moving_variance)
        
        return mean, variance

    return moving_mean, moving_variance

# Doesn't work with TPUEstimator spec
def _accumulated_moments_for_inference(mean, variance, is_training):
    
    variable_collections = [
        tf.GraphKeys.MODEL_VARIABLES, tf.GraphKeys.GLOBAL_VARIABLES,
    ]

    with tf.variable_scope("accu", values=[mean, variance]):
        # Create variables for accumulating batch statistic and use them during
        # inference. The ops for filling the accumulators must be created and run
        # before eval. See docstring above.
        accu_mean = tf.get_variable(
            "accu_mean",
            shape=mean.shape,
            initializer=tf.zeros_initializer(),
            trainable=False,
            collections=variable_collections)
        
        accu_variance = tf.get_variable(
            "accu_variance",
            shape=variance.shape,
            initializer=tf.zeros_initializer(),
            trainable=False,
            collections=variable_collections)
        
        accu_counter = tf.get_variable(
            "accu_counter",
            shape=[],
            initializer=tf.initializers.constant(1e-12),
            trainable=False,
            collections=variable_collections)
        
        update_accus = tf.get_variable(
            "update_accus",
            shape=[],
            dtype=tf.int32,
            initializer=tf.zeros_initializer(),
            trainable=False,
            collections=variable_collections)

        mean = tf.identity(mean, "mean")
        variance = tf.identity(variance, "variance")

        if is_training:
            return mean, variance

        # Return the accumulated batch statistics and add current batch statistics
        # to accumulators if update_accus variables equals 1.
        def update_accus_fn():
            return tf.group([
                tf.assign_add(accu_mean, mean),
                tf.assign_add(accu_variance, variance),
                tf.assign_add(accu_counter, 1),
            ])
        
        dep = tf.cond(
            tf.equal(update_accus, 1),
            update_accus_fn,
            tf.no_op)
        
        with tf.control_dependencies([dep]):
            return accu_mean / accu_counter, accu_variance / accu_counter

def no_batch_norm(inputs):
    return inputs

# Batch norm
def batch_norm(inputs, 
               scope, 
               use_moving_averages, 
               is_training,
               use_tpu_cross_replica_mean,
               decay=0.999,
               scale=False,
               offset=False,
               data_format='NHWC'):

    # Normalize on NHW axes
    inputs = tf.convert_to_tensor(inputs)
    input_shape = inputs.shape
    input_rank = input_shape.ndims
    dtype = inputs.dtype.base_dtype
    num_channels = input_shape[-1].value

    if input_rank not in [2,4]:
        raise ValueError('Input rank must be 2 or 4 rank %d is provided' %
                        (input_rank))
    
    # Reshape 2-D input tensor to 4-D
    if input_rank == 2:
        new_shape = [-1, 1, 1, num_channels] 
        if data_format == 'NCHW':
            new_shape = [-1, num_channels, 1, 1]
        inputs = tf.reshape(inputs, new_shape) 

    axes = 3 if data_format == 'NHWC' else 1
    reduction_axes = [i for i in range(4) if i != axes]

    if use_tpu_cross_replica_mean:
        # Use tpu ops for cross replica mean
        mean, variance = tpu_ops.cross_replica_moments(inputs, reduction_axes)
        pass
    else:
        counts, mean_ss, variance_ss, _ = tf.nn.sufficient_statistics(
            inputs, reduction_axes, keep_dims=False)
        mean, variance = tf.nn.normalize_moments(
            counts, mean_ss, variance_ss, shift=None)


    with tf.variable_scope(scope, values=[inputs, mean, variance]):
        
        if use_moving_averages:
            # Calculate moving average from the mean and variance calculated from the batch
            mean, variance = _moving_moments_for_inference(mean, variance, is_training, decay)
        else:
            # Directly use the mean if training else accumulate mean
            mean, variance = _accumulated_moments_for_inference(mean, variance, is_training)

        collections = [tf.GraphKeys.MODEL_VARIABLES,
                       tf.GraphKeys.GLOBAL_VARIABLES]
        gamma, beta = None, None
        if scale:
            gamma = tf.get_variable(name='gamma',
                                    dtype=dtype,
                                    shape=[num_channels],
                                    initializer=tf.ones_initializer(),
                                    collections=collections)
        
        if offset:
            beta = tf.get_variable(name='beta',
                                   dtype=dtype,
                                   shape=[num_channels],
                                   initializer=tf.zeros_initializer(),
                                   collections=collections)

        # Use tensorflow batch normalization for applying mean and variance to the batch
        variance_epsilon = 1e-12 if (dtype != tf.float16 and dtype != tf.bfloat16) else 1e-3
        output = tf.nn.batch_normalization(x=inputs, 
                                       mean=mean, 
                                       variance=variance,
                                       offset=beta,
                                       scale=gamma,
                                       variance_epsilon=variance_epsilon)
    
        # reshape to input shape
        output = tf.reshape(output, input_shape)
        return output

def condition_modulated_batch_norm(inputs, y, use_sn, is_training,
                                   scale=True, offset=True, 
                                   use_bias=False):
    
    if y is None:
        raise ValueError("You must provide labels for conditional modulation.")
    if y.shape.ndims != 2:
        raise ValueError("Conditioning must have rank 2.")
    
    outputs = batch_norm(inputs, scope='batch_norm', 
                         use_moving_averages=False, 
                         use_tpu_cross_replica_mean=True,  
                         is_training=is_training)
    num_channels = inputs.shape[-1].value

    with tf.variable_scope("cbn", values=[inputs, y]):
        if scale:
            gamma = linear_fc(y, num_channels, scope="gamma", use_bias=use_bias,
                           use_sn=use_sn)
            gamma = tf.reshape(gamma, [-1, 1, 1, num_channels])
            outputs *= gamma
        
        if offset:
            beta = linear_fc(y, num_channels, scope="beta", use_sn=use_sn,
                          use_bias=use_bias)
            beta = tf.reshape(beta, [-1, 1, 1, num_channels])
            outputs += beta
        
        return outputs

# For details : https://arxiv.org/abs/1810.01365
def self_modulated_batch_norm(inputs, z, use_sn, is_training,
                              scale=True, offset=True, 
                              num_hidden=32):
    
    if z is None:
        raise ValueError("You must provide z for self modulation.")
    
    outputs = batch_norm(inputs, scope='batch_norm', 
                         use_moving_averages=False, 
                         use_tpu_cross_replica_mean=True,  
                         is_training=is_training)
    num_channels = inputs.shape[-1].value

    with tf.variable_scope("sbn", values=[inputs, z]):
        h = z
        if num_hidden > 0:
            h = linear_fc(h, num_hidden, scope="hidden", use_sn=use_sn)
            h = tf.nn.relu(h)
        
        if scale:
            gamma = linear_fc(h, num_channels, scope="gamma", bias_start=1.0,
                            use_sn=use_sn)
            gamma = tf.reshape(gamma, [-1, 1, 1, num_channels])
            outputs *= gamma
        
        if offset:
            beta = linear_fc(h, num_channels, scope="beta", use_sn=use_sn)
            beta = tf.reshape(beta, [-1, 1, 1, num_channels])
            outputs += beta
        
        return outputs

# Spectral norm
# For details : https://arxiv.org/pdf/1802.05957.pdf [1]
# Closely folloing the Appendix A Algorithm 1 from [1]
def spectral_norm(inputs, scope, singular_value='auto'):
    if len(inputs) < 2:
        raise ValueError('Spectral norm only supports '
                         'inputs with dimensions > 2 %s' %
                         (inputs.shape))

    # convert [Kw, Kh, Cin, Cout] to [Kw* Kh * Cin, Cout]
    w = tf.reshape(inputs, (-1, inputs.shape[-1]))

    # Choose the smallest among the two dimension if in auto
    if singular_value == 'auto':
        singular_value = 'left' if w.shape[0] <= w.shape[-1] else 'right'
    
    u_shape = (w.shape[0],1) if singular_value == 'left' else (1,w.shape[-1])
    dtype = inputs.dtype.base_dtype
    collections = [tf.GraphKeys.MODEL_VARIABLES,
                   tf.GraphKeys.GLOBAL_VARIABLES]
    with tf.variable_scope(scope):
        u_var = tf.get_variable(name='spectral_norm_u',
                                dtype=dtype,
                                shape=u_shape,
                                initializer=tf.initializers.random_normal,
                                collections=collections)
        
        u_temp = u_var
        iter_count = 1
        epsilon = 1e-12 if (dtype != tf.float16 and dtype != tf.float64) else 1e-3
        for _ in range(iter_count):
            if singular_value == 'left':
                v = tf.math.l2_normalize(
                        tf.matmul(tf.transpose(w), u), 
                        axis=None, 
                        epsilon=epsilon)
                u = tf.math.l2_normalize(
                        tf.matmul(w, v), 
                        axis=None, 
                        epsilon=epsilon)
            else: # right
                v = tf.math.l2_normalize(
                        tf.matmul(u, w, transpose_b=True),
                        epsilon=epsilon)
                u = tf.math.l2_normalize(
                        tf.matmul(v, w), 
                        epsilon=epsilon)
        
        with tf.control_dependencies([
                tf.assign(u_var,u_temp,
                          name='spectral_norm_u_update')
                ]):
            u_temp = tf.identity(u_temp)
        
        # Stop gradient
        u = tf.stop_gradient(u)
        v = tf.stop_gradient(v)

        # Calculate normalized weights
        if singular_value == "left":
            norm_value = tf.matmul(tf.matmul(tf.transpose(u), w), v)
        else:
            norm_value = tf.matmul(tf.matmul(v, w), u, transpose_b=True)
        
        # Shape safeguards
        norm_value.shape.assert_is_fully_defined()
        norm_value.shape.assert_is_compatible_with([1,1])

        w_normalized = w/norm_value

        w_normalized = tf.reshape(w_normalized, inputs.shape)
        return w_normalized




# Layer normalization
def layer_norm(inputs,
               scope,
               scale=True,
               offset=True,
               begin_norm_axis=1,
               begin_param_axis=-1
               ):
    '''
    Normalization done over last three axis
    NHWC - HWC or NCHW - CHW
    begin_norm_axis - start axis of batch normalization
    begin_param_axis - start axis for scale and offset 
    '''
    with tf.variable_scope(scope, values=[inputs]):
        # Conversion to tensor for being on the safe side
        # doesn't effect if inputis already a tensor
        inputs = tf.convert_to_tensor(inputs)
        input_shape = inputs.shape
        input_rank = input_shape.ndims
        dtype = inputs.dtype.base_dtype
        if begin_norm_axis < 0:
            begin_norm_axis = input_rank + begin_norm_axis
    
        if begin_norm_axis >= input_rank or begin_param_axis >= input_rank:
            raise ValueError('norm param (%d) or param axis (%d)' 
                            'must be less < than input rank (%d)' % 
                            (begin_norm_axis, begin_param_axis, input_rank))

        param_shape = input_shape[begin_param_axis:]
        if not param_shape.is_fully_defined():
            raise ValueError('Partially know or unknown shape not supported %s' %
                            (param_shape))
    
        collections = [tf.GraphKeys.MODEL_VARIABLES,
                       tf.GraphKeys.GLOBAL_VARIABLES]

        gamma, beta = None, None
        if scale:
            gamma = tf.get_variable(name='gamma',
                                    dtype=dtype,
                                    shape=param_shape,
                                    initializer=tf.ones_initializer(),
                                    collections=collections)
        
        if offset:
            beta = tf.get_variable(name='beta',
                                   dtype=dtype,
                                   shape=param_shape,
                                   initializer=tf.zeros_initializer(),
                                   collections=collections)

        moments_axes = list(range(begin_norm_axis,input_rank))
        mean, variance = tf.nn.moments(inputs, moments_axes, keep_dims=True)

        # uses batch_norm for normalization
        # Note that epsilon must be increased for float16 and bfloat16 due to the limited
        # representable range.
        variance_epsilon = 1e-12 if (dtype != tf.float16 and dtype != tf.bfloat16) else 1e-3
        output = tf.nn.batch_normalization(x=inputs, 
                                  mean=mean, 
                                  variance=variance,
                                  offset=beta,
                                  scale=gamma,
                                  variance_epsilon=variance_epsilon)
        
        output = tf.reshape(output, input_shape)
        return output



# Notes
# http://bit.ly/2TNlhyh
# Graphical visualization
# https://github.com/vdumoulin/conv_arithmetic

# Convolution 2d
def conv2d(inputs, kh, kw, kn, stride_h, stride_w, scope, 
           use_bias=True, stddev=0.02, use_sn=False):
    '''
    kh - kernel height
    kw - kernel width
    kn - number of kernels
    stride_h - stride for height
    stride_w - stride for width
    '''
    input_shape = inputs.shape.as_list()
    with tf.variable_scope(scope, values=[inputs]):
        w = tf.get_variable(name='kernel',
                            shape=[kh, kw, input_shape[-1], kn],
                            initializer=initializer(stddev=stddev))
        
        if use_sn:
            w = spectral_norm(w, scope='spectral_norm')
        
        output = tf.nn.conv2d(inputs, w, 
                              strides=[1,stride_h, stride_w, 1], 
                              padding='SAME')

        if use_bias:
            b = tf.get_variable(name='bias',
                                shape=[kn],
                                initializer=tf.zeros_initializer())
            output += b
        
        return output


# Notes
# Explanation of deconv in detail:
# [1] https://arxiv.org/pdf/1609.07009.pdf
# Backprop in convolution
# [2] http://bit.ly/2Pe91bs

# Generally deconv can be achieved in two ways: 
# 1) By performing convolution on Unpooled sub-pixel image [1]
# 2) By using conv gradient calculation wrt to inputs [2]
# Only difference b/w these two are indices of weights used but if
# weights are learned they will achieve the same result

# General notes on tensorflow
# tf.nn.conv*_transpose uses backprop for calculation

# In this code second method as mentioned above is used along with
# N dimensional unpooling operation taken from
# http://bit.ly/2P4Hk5a

def unpool(value, name="unpool"):
    """Unpooling operation.
    N-dimensional version of the unpooling operation from
    https://www.robots.ox.ac.uk/~vgg/rg/papers/Dosovitskiy_Learning_to_Generate_2015_CVPR_paper.pdf
    Taken from: https://github.com/tensorflow/tensorflow/issues/2169
    Args:
        value: a Tensor of shape [b, d0, d1, ..., dn, ch]
        name: name of the op
    Returns:
       A Tensor of shape [b, 2*d0, 2*d1, ..., 2*dn, ch]
    """
    with tf.name_scope(name) as scope:
        sh = value.get_shape().as_list()
        dim = len(sh[1:-1])
        out = (tf.reshape(value, [-1] + sh[-dim:]))
        for i in range(dim, 0, -1):
            out = tf.concat([out, tf.zeros_like(out)], i)
        out_size = [-1] + [s * 2 for s in sh[1:-1]] + [sh[-1]]
        out = tf.reshape(out, out_size, name=scope)
    return out

# DeConvolution 2d
def deconv2d(inputs, output_shape, 
             kh, kw, stride_h, stride_w, 
             scope, use_sn=False, stddev=0.02):
             
    with tf.variable_scope(scope, values=[inputs]):
        w = tf.get_variable(
                "kernel", [kh, kw, output_shape[-1], inputs.get_shape()[-1]],
                initializer=initializer(stddev=stddev))
        
        if use_sn:
            w = spectral_norm(w, scope='spectral_norm')
        
        deconv = tf.nn.conv2d_transpose(
                    inputs, w, output_shape=output_shape, strides=[1, stride_h, stride_w, 1])

        bias = tf.get_variable(
                    "bias", [output_shape[-1]], initializer=tf.constant_initializer(0.0))

        return tf.reshape(tf.nn.bias_add(deconv, bias), tf.shape(deconv))

# Linear layer or fully connected
def linear_fc(inputs, output_size, scope, use_bias=True, 
              bias_start=0.0, use_sn=False, stddev=0.02):
    input_shape = inputs.shape.as_list()
    with tf.variable_scope(scope, values=[inputs]):
        kernel = tf.get_variable(name='kernel', 
                                 shape=[input_shape[-1], output_size],
                                 initializer=initializer(stddev=stddev))
        if use_sn:
            kernel = spectral_norm(kernel, scope='spectral_norm')
        
        outputs = tf.matmul(inputs, kernel)

        if use_bias:
            bias = tf.get_variable(name='bias',
                                   shape=[output_size],
                                   initializer=tf.constant_initializer(bias_start))
            outputs += bias
        
        return outputs
    pass

# Leaky relu
def leaky_relu(inputs, leak=0.2):
    return tf.maximum(inputs, leak*inputs, name='lrelu')

# For details : https://arxiv.org/abs/1805.08318 [1]
def self_attention(inputs, scope, use_sn=False):
    conv1x1 = functools.partial(conv2d, kh=1, kw=1, stride_h=1, stride_w=1)
    
    def flatten(input):
        shape=input.shape.as_list()
        return tf.reshape(input, shape=(-1, shape[1]* shape[2], shape[3]))

    with tf.variable_scope(scope, values=[inputs]):
        input_h, input_w, num_channels = inputs.shape.as_list()[1:]

        # Two feature spaces for attention  mask calculation
        # [1] tries with output channel count of 1,2,4,8 and noticed no significant 
        # performance decrease when decreasing the output channel
        
        out_channel_count = num_channels // 8
        f = conv1x1(inputs, kn=out_channel_count, use_sn=use_sn, scope='f_conv2d',
                    use_bias=False)
        # flatten, according to paper f and g have shape (N x C)
        f = flatten(f)
        
        g = conv1x1(inputs, kn=out_channel_count, use_sn=use_sn, scope='g_conv2d',
                    use_bias=False)
        # Not in the paper but implemented inside tensorflow compare_gan
        # https://github.com/google/compare_gan
        g = tf.nn.max_pool2d(input=g, ksize=[1,2,2,1], 
                             strides=[1,2,2,1], padding='SAME',
                             name='g_maxpool')
        g = flatten(g)

        # transpose g instead of f as mentioned in the paper 
        fxg = tf.matmul(f,g, transpose_b=True)
        beta = tf.nn.softmax(fxg)

        h = conv1x1(inputs, kn=out_channel_count, use_sn=use_sn, scope='h_conv2d',
                    use_bias=False)
        h = tf.nn.max_pool2d(input=h, ksize=[1,2,2,1], 
                     strides=[1,2,2,1], padding='SAME',
                     name='g_maxpool')
        h = flatten(h)

        beta_x_h=tf.matmul(beta, h)
        beta_x_h = tf.reshape(beta_x_h, shape=(-1, input_h, input_w, out_channel_count))

        o = conv1x1(beta_x_h, kn=num_channels, use_sn=use_sn, scope='v_conv2d',
                    use_bias=False)
        
        # At start scalar is zero so that the network can first rely on local neighborhood cues
        gamma = tf.get_variable(name='gamma',
                                shape=[],
                                initializer=tf.zeros_initializer())
        
        return gamma * o + inputs
        

 
    
