# BIG-GAN implementation
# [1] - https://arxiv.org/pdf/1809.11096.pdf

# Generator 
# Class-conditioned batch norm from [2], [3]
# [2] - https://arxiv.org/pdf/1610.07629.pdf
# [3] - https://arxiv.org/pdf/1707.00683.pdf
# Spectral norm 
# Self attention


# Discriminator 
# Class information with projection from [4]
# [4] - https://arxiv.org/pdf/1802.05637.pdf
# Spectral norm
# Self attention

# Uses orthogonal initialization

# Optimization
# Optimization setting follows closely follows [5] 
# [5] uses separate learning rates from [6],

# Spectral norm [7] for both generator and discriminator
# whereas [7] uses it only for discriminator 
# [5] - https://arxiv.org/abs/1805.08318
# [6] - https://arxiv.org/pdf/1706.08500.pdf
# [7] - https://arxiv.org/pdf/1802.05957.pdf

# [5] uses Adam optimizer with β1 = 0 and β2 = 0.9 for training and the learning
# rate of 0.0004 and 0.0001 for the discriminator and the generator respectively

# [5] uses 1:1 step ratio for G/D training 
# Self-attention blocks gave best performance on 32x32 for FID - 18.28, IS - 51.43 
# and 64x64 for IS -  52.52, FID - 18.65

# In this implementation according to [1] 
# halve learning rates and take two D steps per G step are taken

# For evaluation using EMA for generator weights 

# --------
# BigGAN for TPU only
# --------

import tensorflow as tf
import numpy as np
import functools

import tf_ops

conv1x1 = functools.partial(tf_ops.conv2d, kh=1, kw=1, use_bias=True)
conv3x3 = functools.partial(tf_ops.conv2d, kh=3, kw=3, use_bias=True)

# TPU dependent 
batch_norm = functools.partial(tf_ops.condition_modulated_batch_norm,
                               scale=True,
                               offset=True,
                               use_bias=False)

def get_conv(inputs, ksize, out_channels, stride, scope, use_sn, scale):
    
    output = inputs

    if scale == 'up':
        output = tf_ops.unpool(output)

    output = tf_ops.conv2d(inputs=output, kn=out_channels,
                           kh=ksize, kw=ksize, use_bias=True, 
                           stride_h=stride, stride_w=stride,
                           use_sn=True, scope=scope)
        
    if scale == 'down':
        output = tf.nn.pool(output, [2,2], 'AVG',
                            padding='SAME', strides=[2,2], 
                            name='{}_{}'.format(scope, 'avgpool'))
    
    return output

def resnet_block(inputs, embedded_z ,out_channels, use_sn,
                 strides, scope, gen_block, scale,
                 is_training, use_skip_connection):
    with tf.variable_scope(scope, values=[inputs]):

        scale1 = scale if gen_block else None
        scale2 = None if gen_block else scale 
        
        output = inputs

        # Part 1
        # Conditional batch norm
        output = batch_norm(output, embedded_z, use_sn=False, is_training=is_training)
        output = tf.nn.relu(output)

        output = get_conv(inputs=output, out_channels=out_channels, 
                          ksize=3, stride=strides, scale=scale1,
                          use_sn=True, scope='conv2d_1')

        # Part 2
        # Conditional batch norm
        output = batch_norm(output, embedded_z, use_sn=False, is_training=is_training)
        output = tf.nn.relu(output)

        output = get_conv(inputs=output, out_channels=out_channels, 
                          ksize=3, stride=strides, scale=scale2,
                          use_sn=True, scope='conv2d_2')

        # Shortcut
        if use_skip_connection:
            shortcut = get_conv(inputs=inputs, out_channels=out_channels, 
                                ksize=1, stride=strides, scale=scale,
                                use_sn=True, scope='conv2d_shortcut')
            
            output += shortcut
        
        return output

class BigGANGenerator:
    def __init__(self, use_sn, resolution, ch_multiplier, attn_block):
        self.use_sn = use_sn
        self.resolution = resolution
        self.ch_multiplier = ch_multiplier
        self.attn_block = attn_block

    def get_in_out_channels(self):
        if self.resolution = 512:
            return [16, 16, 8, 8, 4, 2, 1, 1]
        elif self.resolution == 256:
            return [16, 16, 8, 8, 4, 2, 1]
        elif self.resolution == 128:
            return [16, 16, 8, 4, 2, 1]

    # Creates a generator for 128x128
    def __call__(self, input, z, y, is_training):

        in_out_channels = self.get_in_out_channels()
        in_channels = [self.ch_multiplier * c for c in in_out_channels[:-1]]
        out_channels = [self.ch_multiplier * c for c in in_out_channels[1:]]

        block_count = len(out_channels)

        

        pass

def discriminator():
    # Create a discriminator for 128x128
    pass
