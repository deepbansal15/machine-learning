#!/bin/bash
nohup python3 tpu_imagenet.py --resnet_depth=50 --tpu_name=tpu-vm-v3-2 \
--model_dir=gs://tpu-gke-bucket/Imagenet/resnet/resnet-50-mvAvg > \
./logs/resnet_50_mv_avg.log &

nohup python3 tpu_imagenet.py --resnet_depth=101 --tpu_name=tpu-vm-v3-3 \
--model_dir=gs://tpu-gke-bucket/Imagenet/resnet/resnet-101-mvAvg > \
./logs/resnet_101_mv_avg.log &

nohup python3 tpu_imagenet.py --resnet_depth=152 --tpu_name=tpu-vm-v3-4 \
--model_dir=gs://tpu-gke-bucket/Imagenet/resnet/resnet-152-mvAvg > \
./logs/resnet_152_mv_avg.log &

nohup python3 tpu_imagenet.py --resnet_depth=34 --tpu_name=tpu-vm-v3-5 \
--model_dir=gs://tpu-gke-bucket/Imagenet/resnet/resnet-34-mvAvg > \
./logs/resnet_34_mv_avg.log &

nohup python3 tpu_imagenet.py --resnet_depth=200 --tpu_name=tpu-vm-v3-1 \
--model_dir=gs://tpu-gke-bucket/Imagenet/resnet/resnet-200-mvAvg > \
./logs/resnet_200_mv_avg.log &
