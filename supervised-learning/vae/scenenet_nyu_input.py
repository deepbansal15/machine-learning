import os
import tensorflow as tf

WIDTH = 320
HEIGHT = 240

# tf.data.TFRecordDataset
PREFETCH_BUFFER_SIZE = 8 * 1024 * 1024
NUM_FILES_INFEED = 8
SHUFFLE_BUFFER_SIZE = 1000

class ScenenetNYUInput:
    def __init__(self, data_dir=None):
        self.data_dir = data_dir
    
    # Parse a single example protobuffer inside tfrecord and apply a preprocess function on it
    def dataset_parser(self, value):
        features = {'image/encoded': tf.FixedLenFeature((), tf.string, ''),
                    'depth/encoded': tf.FixedLenFeature((), tf.string, '')}
    
        parsed = tf.parse_single_example(value, features)

        # Feels like this is un-necessary need to check by directly
        # changing features from () => []
        rgb_bytes = tf.reshape(parsed['image/encoded'], shape=[])
        rgb_image = tf.io.decode_jpeg(rgb_bytes)
        # rgb_image = tf.image.transpose(rgb_image)
        rgb_image = tf.reshape(rgb_image, [HEIGHT, WIDTH, 3])
        rgb_image = tf.cast(rgb_image, tf.float32)
        rgb_image /= 255.0

        depth_bytes = tf.reshape(parsed['depth/encoded'], shape=[])
        depth_image = tf.io.decode_png(depth_bytes, dtype=tf.uint16)
        # fixes the shape for TPU
        # depth_image = tf.image.transpose(depth_image)
        depth_image = tf.reshape(depth_image, [HEIGHT, WIDTH, 1])
        depth_image = tf.cast(depth_image, tf.float32)
        # 15 m as max depth
        depth_image /= 15000.0

        return {'rgb':rgb_image, 'depth':depth_image}, tf.constant(0, tf.int32)

    def __call__(self,params):
        # Retrieves the batch size for the current shard. The # of shards is
        # computed according to the input pipeline deployment. See
        # `tf.contrib.tpu.RunConfig` for details.
        batch_size = params['batch_size']

        # Shuffle the filenames to ensure better randomization
        file_pattern = os.path.join(self.data_dir, 'train/train-*')
        dataset = tf.data.Dataset.list_files(file_pattern, shuffle=False)
        dataset = dataset.shuffle(buffer_size=64)  # 64 files in dataset
        dataset = dataset.repeat()

        def prefetch_dataset(filename):
            buffer_size = PREFETCH_BUFFER_SIZE
            dataset = tf.data.TFRecordDataset(filename, buffer_size=buffer_size)
            return dataset

        dataset = dataset.apply(
            tf.contrib.data.parallel_interleave(
                prefetch_dataset, cycle_length=NUM_FILES_INFEED, sloppy=True))
        dataset = dataset.shuffle(SHUFFLE_BUFFER_SIZE)

        dataset = dataset.map(self.dataset_parser, num_parallel_calls=128)
        dataset = dataset.prefetch(batch_size)
        dataset = dataset.batch(batch_size, drop_remainder=True)
        dataset = dataset.prefetch(2)  # Prefetch overlaps in-feed with training
        return dataset


def test_dataset():
    import imageio
    params = {'batch_size':64}
    dataset = ScenenetNYUInput('gs://tpu-gke-bucket/nyu_scenenet/')(params)
    print(dataset.output_shapes)
    iterator = dataset.make_initializable_iterator()
    next_element = iterator.get_next()
    with tf.Session() as sess:
        sess.run([iterator.initializer])
        while True:
            try:
                random_vector = tf.random.normal(shape=[64, 240, 320, 3])
                cross_ent_rgb = tf.nn.sigmoid_cross_entropy_with_logits(
                    logits=random_vector, labels=next_element[0]['rgb'])
                pk = -tf.reduce_sum(cross_ent_rgb, axis=[1, 2, 3])
                ent, mean = sess.run([cross_ent_rgb, pk])

                print(mean)
                # print(ent)
                # print('depth shape : {}, rgb shape : {}'.format(
                        # value[0]['depth'].get_shape(),value[0]['rgb'].get_shape()))
                # max_depth_val = tf.math.reduce_max(value['depth'])
                # min_depth_val = tf.math.reduce_min(value['depth'])

                # val_max, val_min = sess.run([max_depth_val,min_depth_val])
                # print(val_max, val_min)
                # print(value[0]['rgb'].shape)
                # imageio.imwrite('sample.jpg',value[0]['rgb'][0])
                # imageio.imwrite('sample_depth.png',value[0]['depth'][0])
                break
            except tf.errors.OutOfRangeError:
                print('Out of range exception')
                break

# test_dataset()