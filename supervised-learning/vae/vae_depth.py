import os
import time
import random

import tensorflow as tf
import numpy as np

from scenenet_nyu_input import ScenenetNYUInput
from vae_model import VAEModel

from absl import app
from absl import flags

flags.DEFINE_integer('train_batch_size', 8, 'Batch size for tpu it will be batch_size * TPU Cores')
flags.DEFINE_bool('use_tpu', True, 'Whether to use TPU for training.')
flags.DEFINE_string('work_dir', 'gs://tpu-gke-bucket/vae/vae_depth_model2',
                    'The Estimator working directory. Used to dump: '
                    'checkpoints, tensorboard logs, etc..')
# Takes the filename as well
flags.DEFINE_string('export_dir', 'gs://tpu-gke-bucket/vae/vae_depth_exported_model/model',
                    'Model export directory')
flags.DEFINE_string('data_dir', 'gs://tpu-gke-bucket/nyu_scenenet/','Data dir')
# flags.DEFINE_integer('summary_steps', 100,'Number of steps between logging summary scalars.')
flags.DEFINE_integer('keep_checkpoint_max', 2, 'Number of checkpoints to keep.')

ITERATION_PER_LOOP = 1000
TOTAL_STEPS = 100000

FLAGS = flags.FLAGS

def reparameterize(mean, logvar):
    eps = tf.random.normal(shape=mean.shape)
    return eps * tf.exp(logvar * .5) + mean

def log_normal_pdf(sample, mean, logvar, raxis=1):
  log2pi = tf.math.log(2. * np.pi)
  return tf.reduce_sum(
      -.5 * ((sample - mean) ** 2. * tf.exp(-logvar) + logvar + log2pi),
      axis=raxis)

def model_fn(features, labels, mode, params):
    model = VAEModel()

    rgb_img = features['rgb']

    latent_code = model.encoder(rgb_img)
    mean, logvar = tf.split(latent_code, num_or_size_splits=2, axis=1)
    z = reparameterize(mean, logvar)
    x_logit = model.decoder(z)
    # rgb_logit, depth_logit = tf.split(x_logit, [3,1], axis=-1)

    cross_ent_rgb = tf.nn.sigmoid_cross_entropy_with_logits(logits=x_logit, labels=rgb_img)
    # cross_ent_depth = tf.nn.sigmoid_cross_entropy_with_logits(logits=depth_logit, labels=depth_img)

    logpx_z_rgb = -tf.reduce_sum(cross_ent_rgb, axis=[1, 2, 3])
    # logpx_z_depth = -tf.reduce_sum(cross_ent_depth, axis=[1, 2, 3])

    logpz = log_normal_pdf(z, 0., 0.)
    logqz_x = log_normal_pdf(z, mean, logvar)

    loss = -(logpx_z_rgb + logpz - logqz_x)
    mean_loss = tf.reduce_mean(loss)
    global_step = tf.train.get_or_create_global_step()

    optimizer = tf.train.AdamOptimizer(1e-4)
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    
    if params['use_tpu']:
        optimizer = tf.contrib.tpu.CrossShardOptimizer(optimizer)
    
    with tf.control_dependencies(update_ops):
        train_op = optimizer.minimize(loss, global_step=global_step)

    def metric_fn(loss):
        elbo = tf.metrics.mean(-loss)
        # ce = tf.metrics.mean(ce_repeat)
        print(elbo)
        tf.logging.info(msg='ELBO : {}'.format(elbo))
        return {'elbo':elbo}
        
    # metrics = (metric_fn,[loss])
    # host_call_fn = (metric_fn, [loss])
    estimator_spec = tf.estimator.tpu.TPUEstimatorSpec(
        mode=mode,loss=mean_loss, train_op=train_op)
    
    if params['use_tpu']:
        return estimator_spec
    else:
        return estimator_spec.as_estimator_spec()

def _tpu_estimator():
    tpu_cluster_resolver = tf.contrib.cluster_resolver.TPUClusterResolver(
    'tpu-vm-v3-1', zone=None, project=None)
    tpu_grpc_url = tpu_cluster_resolver.get_master()

    run_config = tf.estimator.tpu.RunConfig(
        master=tpu_grpc_url,
        evaluation_master=tpu_grpc_url,
        model_dir=FLAGS.work_dir,
        save_checkpoints_steps=ITERATION_PER_LOOP,
        keep_checkpoint_max=FLAGS.keep_checkpoint_max,
        session_config=tf.ConfigProto(
            allow_soft_placement=True, log_device_placement=True),
        tpu_config=tf.estimator.tpu.TPUConfig(
            iterations_per_loop=ITERATION_PER_LOOP,
            num_shards=8,
            per_host_input_for_training=tf.estimator.tpu.InputPipelineConfig.PER_HOST_V2))

    # Batch size here is global batch size
    return tf.estimator.tpu.TPUEstimator(
        use_tpu=FLAGS.use_tpu,
        model_fn=model_fn,
        config=run_config,
        train_batch_size=FLAGS.train_batch_size * 8,
        eval_batch_size=FLAGS.train_batch_size * 8,
        params=FLAGS.flag_values_dict())

def export_model():
    export_path = FLAGS.export_dir
    estimator = tf.estimator.Estimator(model_fn, model_dir=FLAGS.work_dir,
                                       params=FLAGS.flag_values_dict())
    latest_checkpoint = estimator.latest_checkpoint()
    all_checkpoint_files = tf.gfile.Glob(latest_checkpoint + '*')
    for filename in all_checkpoint_files:
        suffix = filename.partition(latest_checkpoint)[2]
        destination_path = export_path + suffix
        print("Copying {} to {}".format(filename, destination_path))
        tf.gfile.Copy(filename, destination_path)

def train():
    # Train using estimator
    # Move checkpoints to model dir
    estimator = _tpu_estimator()
    estimator.train(
        input_fn=ScenenetNYUInput(FLAGS.data_dir),
        max_steps=TOTAL_STEPS)
    export_model()
    pass

def save_images(predictions):
    import imageio
    imageio.imwrite('sample_depth.jpg',predictions[0][0])

def cpu_inference(save_dir):
    latent_dim = 200
    num_examples_to_generate = 4

    sess = tf.Session(graph=tf.Graph())
    with sess.graph.as_default():
        random_vector_for_generation = tf.random.normal(
            shape=[num_examples_to_generate, latent_dim])
        vae = VAEModel()
        # if save_dir is not None:
            # tf.train.Saver().restore(sess, save_dir)
        # else:
            # print('session run')
        sess.run(tf.global_variables_initializer())
        
        predictions = vae.decoder(random_vector_for_generation)
        predictions = sess.run([predictions])
        np_array = np.asarray(predictions, dtype=np.float32)
        print(np_array.shape)
        save_images(np_array)

    sess.close()

def main(argv):
    del(argv)
    #train()
    # export_model()
    cpu_inference(FLAGS.export_dir)

if __name__ == '__main__':
    app.run(main)