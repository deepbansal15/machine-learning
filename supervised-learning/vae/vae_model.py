'''
VAE Model
With 2 decoder outputs for depth and rgb

TODO : Actual loss calculation
Input function
Final code for TPU
Seperate inference code
TPU Summary for image output

# References 
[1]: Diederik Kingma and Max Welling. Auto-Encoding Variational Bayes. In
     _International Conference on Learning Representations_, 2014.
     https://arxiv.org/abs/1312.6114
[2]: Yuri Burda, Roger Grosse, Ruslan Salakhutdinov. Importance Weighted
     Autoencoders. In _International Conference on Learning Representations_,
     2015.
     https://arxiv.org/abs/1509.00519

'''

import tensorflow as tf
import numpy as np

import tensorflow_probability as tfp

tfd = tfp.distributions

import functools

def create_encoder(activation, latent_size, base_depth, input_shape):
    tfl = tf.keras.layers

    conv = functools.partial(tfl.Conv2D, padding='SAME', activation=activation)
    encoder_net = tf.keras.Sequential([
        tfl.InputLayer(input_shape=input_shape),
        conv(base_depth, 5, 2),
        conv(base_depth, 5, 2),
        conv(2 * base_depth, 5, 2),
        conv(2 * base_depth, 5, 2),
        conv(4 * latent_size, 7,2, padding="VALID"),
        tfl.Flatten(),
        tfl.Dense(2 * latent_size, activation=None),
    ])
    print(encoder_net.summary())
    return encoder_net

def create_decoder(latent_dim, start_shape):
    tfl = tf.keras.layers
    inputs = tf.keras.Input(shape=(latent_dim,))
    x = tfl.Dense(units=start_shape[0]*start_shape[1]*32, activation=tf.nn.relu)(inputs)
    x = tfl.Reshape(target_shape=(start_shape[0], start_shape[1], 32))(x)
    x = tfl.Conv2DTranspose(
        filters=64,
        kernel_size=3,
        strides=(2, 2),
        padding="SAME",
        activation='relu')(x)
    x = tfl.Conv2DTranspose(
        filters=32,
        kernel_size=3,
        strides=(2, 2),
        padding="SAME",
        activation='relu')(x)
    x = tfl.Conv2DTranspose(
        filters=16,
        kernel_size=3,
        strides=(2, 2),
        padding="SAME",
        activation='relu')(x)
    x = tfl.Conv2DTranspose(
        filters=3,
        kernel_size=3,
        strides=(2, 2),
        padding="SAME",
        activation='relu')(x)
    # x = tfl.Conv2DTranspose(
    #     filters=4,
    #     kernel_size=3,
    #     strides=(2, 2),
    #     padding="SAME",
    #     activation='relu')(x)
    
    # rgb_x,depth_x = tf.split(x, [3,1], axis=-1)
    # rgb_x = tfl.Conv2DTranspose(
    #     filters=3,
    #     kernel_size=3,
    #     strides=(2, 2),
    #     padding="SAME",
    #     activation='relu')(rgb_x)

    # depth_x = tfl.Conv2DTranspose(
    #     filters=1,
    #     kernel_size=3,
    #     strides=(2, 2),
    #     padding="SAME",
    #     activation='relu')(depth_x)

    model = tf.keras.Model(inputs = inputs, outputs=x)
    print(model.summary())
    return model

class VAEModel:
    def __init__(self):
        self.encoder = create_encoder(tf.nn.relu, 200, 32, input_shape=(240,320,3))
        self.decoder = create_decoder(200,(15,20))

def model_test():
    encoder = create_encoder(tf.nn.relu, 200, 32, input_shape=(320,240,3))
    decoder = create_decoder(200,(20,15))

    image_ph = tf.placeholder(tf.float32,(None,320,240,3))
    depth_ph = tf.placeholder(tf.float32,(None,320,240,1))

    latent_code = encoder(image_ph)
    image_generated = decoder(latent_code)

    optimizer = tf.train.AdamOptimizer(1e-4)
    ent_rgb = tf.nn.sigmoid_cross_entropy_with_logits(logits = image_generated[0],labels=image_ph)
    ent_depth = tf.nn.sigmoid_cross_entropy_with_logits(logits = image_generated[1],labels=depth_ph)

    loss_rgb = -tf.reduce_sum(ent_rgb,axis=[1,2,3])
    loss_depth = -tf.reduce_sum(ent_depth,axis=[1,2,3])

    total_loss = loss_rgb + loss_depth
    train_op = optimizer.minimize(total_loss)

    rgb_input = np.random.rand(2,320,240,3)
    depth_input = np.random.rand(2,320,240,1)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        # loss = sess.run([train_op],
        #                        feed_dict={image_ph:rgb_input, depth_ph:depth_input})
        # print(loss)
        code, gen_image = sess.run([latent_code, image_generated],
                               feed_dict={image_ph:rgb_input, depth_ph:depth_input})
        print(code.shape)
        print(gen_image[0].shape)
        print(gen_image[1].shape)


# model_test()

    




