# Trained model with TPU works on CPU
# Train GAN on TPU and use with the drone

import os
import time
import random

import tensorflow as tf
import numpy as np

from absl import app
from absl import flags

from mnist_dataset import mnist_dataset

flags.DEFINE_integer('train_batch_size', 64, 'Batch size for tpu it will be batch_size * TPU Cores')
flags.DEFINE_bool('use_tpu', True, 'Whether to use TPU for training.')
flags.DEFINE_string('work_dir', 'gs://tpu-gke-bucket/mnist/mnist_model',
                    'The Estimator working directory. Used to dump: '
                    'checkpoints, tensorboard logs, etc..')
flags.DEFINE_string('data_dir', 'gs://tpu-gke-bucket/mnist/','Data dir')
# flags.DEFINE_integer('summary_steps', 100,'Number of steps between logging summary scalars.')
flags.DEFINE_integer('keep_checkpoint_max', 2, 'Number of checkpoints to keep.')

ITERATION_PER_LOOP = 100
TOTAL_STEPS = 5000

FLAGS = flags.FLAGS

def create_model():
    input_shape = [28, 28, 1]

    l = tf.keras.layers
    max_pool = l.MaxPooling2D((2, 2), (2, 2), padding='same', data_format='channels_last')
    return tf.keras.Sequential([
        l.Reshape(target_shape=input_shape,input_shape=(28 * 28,)),
        l.Conv2D(32,5,padding='same',data_format='channels_last',activation=tf.nn.relu),
        max_pool,
        l.Conv2D(64,5,padding='same',data_format='channels_last',activation=tf.nn.relu),
        max_pool,
        l.Flatten(),
        l.Dense(1024, activation=tf.nn.relu),
        l.Dropout(0.4),
        l.Dense(10)])

def get_inference_inputs():
    return ({'image':tf.placeholder(tf.float32,shape=(None,784), name='input_image')},
            tf.placeholder(tf.float32,shape=(None,10), name='labels'))


class MNISTModel:
    def __init__(self, save_dir):
        self.save_dir = save_dir
        self.inference_input = None
        self.inference_output = None
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(graph=tf.Graph(), config=config)
        self.initialize_graph()

    
    def initialize_graph(self):
        with self.sess.graph.as_default():
            inputs = get_inference_inputs()
            features, labels = inputs
            params = FLAGS.flag_values_dict()
            params['use_tpu'] = False
            estimator_spec = model_fn(features, labels, tf.estimator.ModeKeys.PREDICT, params)

            self.inference_input = features
            self.inference_output = estimator_spec.predictions
            if self.save_dir is not None:
                self.initialize_weights()
            else:
                print('session run')
                self.sess.run(tf.global_variables_initializer())
    
    def initialize_weights(self):
        tf.train.Saver().restore(self.sess, self.save_dir)

def model_fn(features, labels, mode, params):
    model = create_model()
    
    image = features['image']
    logits = model(image)

    predictions = {
        'classes': tf.argmax(logits, axis=1),
        'probabilities': tf.nn.softmax(logits),
    }

    global_step = tf.train.get_or_create_global_step()
    loss = tf.losses.softmax_cross_entropy(onehot_labels=labels, logits=logits)
    optimizer = tf.train.AdamOptimizer(1e-4)
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    
    if params['use_tpu']:
        optimizer = tf.contrib.tpu.CrossShardOptimizer(optimizer)
    
    with tf.control_dependencies(update_ops):
        train_op = optimizer.minimize(loss, global_step=global_step)

    # Metrics are missing
    estimator_spec = tf.estimator.tpu.TPUEstimatorSpec(mode=mode, 
        predictions=predictions, loss=loss, train_op=train_op)
    
    if params['use_tpu']:
        return estimator_spec
    else:
        return estimator_spec.as_estimator_spec()
    
def _non_tpu_estimator():
    session_config = tf.ConfigProto()
    session_config.gpu_options.allow_growth = True
    run_config = tf.estimator.RunConfig(
        keep_checkpoint_max=FLAGS.keep_checkpoint_max,
        session_config=session_config)
    return tf.estimator.Estimator(
        model_fn,
        model_dir=FLAGS.work_dir,
        config=run_config,
        params=FLAGS.flag_values_dict())

def _tpu_estimator():
    tpu_cluster_resolver = tf.contrib.cluster_resolver.TPUClusterResolver(
    'tpu-vm-v3-1', zone=None, project=None)
    tpu_grpc_url = tpu_cluster_resolver.get_master()

    run_config = tf.estimator.tpu.RunConfig(
        master=tpu_grpc_url,
        evaluation_master=tpu_grpc_url,
        model_dir=FLAGS.work_dir,
        save_checkpoints_steps=ITERATION_PER_LOOP,
        keep_checkpoint_max=FLAGS.keep_checkpoint_max,
        session_config=tf.ConfigProto(
            allow_soft_placement=True, log_device_placement=True),
        tpu_config=tf.estimator.tpu.TPUConfig(
            iterations_per_loop=ITERATION_PER_LOOP,
            num_shards=8,
            per_host_input_for_training=tf.estimator.tpu.InputPipelineConfig.PER_HOST_V2))

    # Batch size here is global batch size
    return tf.estimator.tpu.TPUEstimator(
        use_tpu=FLAGS.use_tpu,
        model_fn=model_fn,
        config=run_config,
        train_batch_size=FLAGS.train_batch_size * 8,
        eval_batch_size=FLAGS.train_batch_size * 8,
        params=FLAGS.flag_values_dict())

def maybe_set_seed():
    random.seed(0)
    tf.set_random_seed(0)
    np.random.seed(0)

def bootstrap():
    maybe_set_seed()
    initial_checkpoint_name = 'model.ckpt-1'
    save_file = os.path.join(FLAGS.work_dir, initial_checkpoint_name)
    params = FLAGS.flag_values_dict()
    params['use_tpu'] = False
    sess = tf.Session(graph=tf.Graph())
    with sess.graph.as_default():
        features, labels = get_inference_inputs()
        model_fn(features, labels, tf.estimator.ModeKeys.PREDICT,
                 params=params)
        sess.run(tf.global_variables_initializer())
        tf.train.Saver().save(sess, save_file)

def input_fn(params):
    # Feed input MNIST dataset
    batch_size = params['batch_size']
    print('input function batch size {}'.format(batch_size))
    data_dir = params["data_dir"]
    # Retrieves the batch size for the current shard. The # of shards is
    # computed according to the input pipeline deployment. See
    # `tf.contrib.tpu.RunConfig` for details.
    ds = mnist_dataset.train(data_dir).cache().repeat().shuffle(buffer_size=50000).batch(batch_size, drop_remainder=True)
    return ds

def train():
    # Train using estimator
    # Move checkpoints to model dir
    estimator = _tpu_estimator()
    estimator.train(
        input_fn=input_fn,
        max_steps=TOTAL_STEPS)
    pass

def export_model(model_path):
    estimator = tf.estimator.Estimator(model_fn, model_dir=FLAGS.work_dir,
                                       params=FLAGS.flag_values_dict())
    # return gs://tpu-gke-bucket/mnist/mnist_model/model.ckpt-5000
    latest_checkpoint = estimator.latest_checkpoint()
    all_checkpoint_files = tf.gfile.Glob(latest_checkpoint + '*')
    for filename in all_checkpoint_files:
        suffix = filename.partition(latest_checkpoint)[2]
        destination_path = model_path + suffix
        print("Copying {} to {}".format(filename, destination_path))
        tf.gfile.Copy(filename, destination_path)

def predict_cpu():
    # Load checkpoints from model dir
    model_path = 'gs://tpu-gke-bucket/mnist/exported_model/model'
    # export_model(model_path)
    model = MNISTModel(model_path)

    with model.sess as sess:
        dataset = mnist_dataset.train(FLAGS.data_dir).shuffle(buffer_size=10).batch(2).repeat(10)
        iterator = dataset.make_initializable_iterator()
        next_element = iterator.get_next()
        model.sess.run([iterator.initializer])
        while True:
            try:
                value = model.sess.run(next_element)
                features, labels = value
                in_ph = model.inference_input['image']
                print(model.sess.run([model.inference_output], feed_dict={in_ph:features['image']}))
                print(labels)
                break
            except tf.errors.OutOfRangeError:
                print('Out of range exception')
                break
    pass

def main(argv):
    del(argv)
    print(tf.train.list_variables(tf.train.latest_checkpoint(FLAGS.work_dir)))
    # bootstrap()
    # train()
    predict_cpu()

if __name__ == '__main__':
    app.run(main)