import os
import tarfile
import random
import time
import math

import tensorflow as tf
import numpy as np

from google.cloud import storage

'''
the parameters for randomly choosing lighting and trajectories remains
the same. We selected two layouts from each type (bathroom, kitchen, office, living room, and bedroom) for the
validation and test sets making the layout split
'''

TRAINING_DIRECTORY = 'train'
TFRECORD_DIRECTORY = 'tfrecord'
TRAIN_DATA_DIR = 'DIV2K_train_HR'
GCS_BUCKET = 'tpu-gke-bucket'

DIV2K_IMAGES_COUNT = 800


# Create TFRecord and upload to dataset

def check_and_create_dir(directory):
    if not tf.gfile.Exists(directory):
        tf.gfile.MakeDirs(directory)

def int64_list_feature(value):
    if not isinstance(value,list):
        value = [value]
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))

def bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def convert_to_example(filename, image_buffer):
    example = tf.train.Example(features=tf.train.Features(feature={
        'image/encoded': bytes_feature(image_buffer)
    }))
      
    return example

def process_image(filename):
    # Read the image file.
    with tf.gfile.FastGFile(filename, 'rb') as f:
        image_data = f.read()

    # Fixed height and width
    return image_data

def process_images_in_shard(output_file, filenames):
    writer = tf.io.TFRecordWriter(path=output_file)

    for file in filenames:
        print('Added file to the record : {}'.format(file))
        rgb_image_data = process_image(file)
        example = convert_to_example(file, rgb_image_data)
        writer.write(example.SerializeToString())
    
    writer.close()

def process_dataset(filenames, output_directory):
    check_and_create_dir(output_directory)
    output_file = os.path.join(output_directory, 'train_800')
    process_images_in_shard(output_file, filenames)
    print('Finished writing file: %s' % output_file)

    return output_file

def get_training_files(scratch_dir, limit_scenet_images=False):
    # Shuffle training records to ensure we are distributing classes
    # across the batches.
    random.seed(0)
    def make_shuffle_idx(n, elements):
        order = [i for i in range(n)]
        random.shuffle(order)
        order = random.sample(order, elements)
        return order
    
    # Glob all the training files
    hr_training_files = tf.gfile.Glob(
        os.path.join(scratch_dir, TRAIN_DATA_DIR, '*.png'))
    
    shuffle_idx = make_shuffle_idx(len(hr_training_files), len(hr_training_files))
    hr_training_files = [hr_training_files[i] for i in shuffle_idx]
    return hr_training_files

def create_records(scratch_dir):
    training_files = get_training_files(scratch_dir, True)
    print(len(training_files))

    training_records = process_dataset(
        training_files,
        os.path.join(scratch_dir,TFRECORD_DIRECTORY))

def upload_gcs(scratch_dir):
    training_records = tf.gfile.Glob(
        os.path.join(scratch_dir, TFRECORD_DIRECTORY, '*'))
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(GCS_BUCKET)

    def upload_files(records, gcs_blob_path):
        for record_path in records:
            record_name = os.path.basename(record_path)
            destination_blob_name = os.path.join(gcs_blob_path, record_name)

            blob = bucket.blob(destination_blob_name)
            blob.upload_from_filename(record_path)

            print('Finished uploading file: %s' % record_name)
    
    train_cloud_path = os.path.join('div2k', TRAINING_DIRECTORY)
    upload_files(training_records, train_cloud_path)

def upload_images_gcs(scratch_dir):
    training_records = tf.gfile.Glob(
        os.path.join(scratch_dir, TRAIN_DATA_DIR, '*.png'))
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(GCS_BUCKET)

    def upload_files(records, gcs_blob_path):
        for record_path in records:
            record_name = os.path.basename(record_path)
            destination_blob_name = os.path.join(gcs_blob_path, record_name)

            blob = bucket.blob(destination_blob_name)
            blob.upload_from_filename(record_path)

            print('Finished uploading file: %s' % record_name)
    
    train_cloud_path = os.path.join('div2k', TRAINING_DIRECTORY,'images')
    upload_files(training_records, train_cloud_path)

# create_records('/home/deepbansal/div2k')
# upload_gcs('/home/deepbansal/div2k')
upload_images_gcs('/home/deepbansal/div2k')
#get_max_min_depth('/home/deepbansal/nyu_scenenet')

# training_records = create_nyu_records('/home/deepbansal/nyu_scenenet')
# upload_nyu_gcs(training_records)

# Assuming that above code ran once