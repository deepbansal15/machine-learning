import tensorflow as tf
import numpy as np

def session_reset(session):
    try:
        session.close()
    except:
        pass
    tf.reset_default_graph()
    return tf.Session()

tf_session = session_reset(None)

def create_model():
    input_ph = tf.placeholder(dtype=tf.float32,shape=[None,1])
    output_ph = tf.placeholder(dtype=tf.float32,shape=[None,1])

    w0 = tf.get_variable(name='w0',shape=[1,20],initializer=tf.initializers.random_normal,trainable=True)
    w1 = tf.get_variable(name='w1',shape=[20,20],initializer=tf.initializers.random_normal,trainable=True)
    w2 = tf.get_variable(name='w2',shape=[20,1],initializer=tf.initializers.random_normal,trainable=True)

    b0 = tf.get_variable(name='b0',shape=[20],initializer=tf.initializers.constant,trainable=True)
    b1 = tf.get_variable(name='b1',shape=[20],initializer=tf.initializers.constant,trainable=True)
    b2 = tf.get_variable(name='b2',shape=[1],initializer=tf.initializers.constant,trainable=True)

    weights = [w0, w1, w2]
    baises = [b0, b1, b2]
    activations = [tf.nn.relu, tf.nn.relu, None]

    layer = input_ph
    for w,b,a in zip(weights,baises,activations):
        layer = tf.matmul(layer,w) + b
        if a is not None:
            layer = a(layer)

    output_pred = layer    

    return input_ph, output_ph, output_pred

inputs = np.linspace(-2 * np.pi, 2* np.pi, 1000000)[:,None]
outputs = np.sin(inputs) + 0.05 * np.random.normal(size=[len(inputs),1])

input_ph, output_ph, output_pred = create_model()

# Loss
mse_loss = tf.reduce_mean(0.5 * tf.square(output_pred - output_ph))
opt = tf.train.AdamOptimizer().minimize(mse_loss)
tf_session.run(tf.global_variables_initializer())
saver = tf.train.Saver()

batch_size = 32
for training_step in range(1000000):
    indices = np.random.randint(low=0,high=len(inputs),size=batch_size)
    input_batch = inputs[indices]
    output_batch = outputs[indices]

    _, mse_run = tf_session.run([opt,mse_loss],feed_dict={input_ph:input_batch,output_ph:output_batch})

    if training_step % 100000 == 0:
        print('{0:04d} mse: {1:.3f}'.format(training_step,mse_run))
        saver.save(tf_session,'tmp/basic_nn_model.ckpt')
