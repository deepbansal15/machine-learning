import os
import tensorflow as tf

tf.enable_eager_execution()

data_dir = 'gs://tpu-gke-bucket/Imagenet'

file_pattern = os.path.join(data_dir, 'train/train-*')
dataset = tf.data.Dataset.list_files(file_pattern, shuffle=False)
dataset = dataset.shuffle(buffer_size=1024)
dataset = dataset.repeat()

def prefetch_dataset(filename):
    buffer_size = 8 * 1024 * 1024
    dataset = tf.data.TFRecordDataset(filename, buffer_size=buffer_size)
    return dataset

dataset = dataset.apply(
            tf.contrib.data.parallel_interleave(
                prefetch_dataset, cycle_length=8, sloppy=True))



dataset = dataset.shuffle(1000)

def dataset_parser(value):
    features = {'image/encoded': tf.FixedLenFeature((), tf.string, ''),
                'image/class/label': tf.FixedLenFeature([], tf.int64, -1),
                'image/channels': tf.FixedLenFeature([], tf.int64, 3)}

    parsed = tf.parse_single_example(value, features)
    # Feels like this is un-necessary need to check by directly
    # changing features from () => []
    image_bytes = tf.reshape(parsed['image/encoded'], shape=[])
    image = tf.io.decode_jpeg(image_bytes, channels=3)
    image = tf.image.resize_images(image, size=[224, 224], 
                            method=tf.image.ResizeMethod.BICUBIC)
    # Can do one hot encoding here
    image_label = tf.cast(tf.reshape(parsed['image/class/label'],shape=[]), dtype=tf.int32)
    return image, tf.one_hot(image_label, 1001)

dataset = dataset.map(dataset_parser, num_parallel_calls=128)
# dataset = dataset.prefetch(1024)
dataset = dataset.batch(1024, drop_remainder=True)
dataset.cache()

for element in dataset:
    #print(element.dtype)
    print(element[0].shape)
    #print(tf.strings.length(element))
    #string_val = element.numpy()
    #print(string_val.count('image/width'))
    # print(element)
    break
