import os
import tensorflow as tf

tf.enable_eager_execution()

data_dir = 'gs://tpu-gke-bucket/div2k'

file_pattern = os.path.join(data_dir, 'train/images/*.png')
dataset = tf.data.Dataset.list_files(file_pattern, shuffle=False)

def load_and_preprocess(path):
    img_raw = tf.io.read_file(path)
    image = tf.io.decode_png(img_raw)
    image = tf.image.resize_images(image, size=[1356, 2040], 
              method=tf.image.ResizeMethod.BICUBIC)
    return image

# Try to use autotune here
dataset = dataset.map(load_and_preprocess, num_parallel_calls=128)
# dataset = dataset.batch(800)
dataset.cache()

dataset = dataset.shuffle(400)
# dataset = dataset.repeat()


# def dataset_parser(value):
#     features = {'image/encoded': tf.FixedLenFeature((), tf.string, '')}

#     parsed = tf.parse_example(value, features)
#     # Feels like this is un-necessary need to check by directly
#     # changing features from () => []
#     image_bytes = tf.reshape(parsed['image/encoded'], shape=[800,])
#     #image = tf.io.decode_png(image_bytes)
#     #image = tf.image.resize_images(image, size=[1356, 2040], 
#     #           method=tf.image.ResizeMethod.BICUBIC)
#     # Can do one hot encoding here
#     return image_bytes, 1

# #dataset = dataset.map(dataset_parser, num_parallel_calls=128)
dataset = dataset.batch(10)
dataset = dataset.prefetch(2)
# dataset.cache()

# datset = dataset.map(dataset_parser, num_parallel_calls=128)
# dataset.cache()

i = 0
for element in dataset:
    print(element.dtype)
    print(element.shape)
    #print(tf.strings.length(element))
    #string_val = element.numpy()
    #print(string_val.count('image/width'))
    # print(element)
    if i == 8:
        break
    i += 1
