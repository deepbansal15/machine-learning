# Create and upload downloaded Imagenet dataset TFRecords to GCS

import os
import random
import math
import tarfile
from urllib.request import urlretrieve

import tensorflow as tf
import numpy as np

from absl import app
from absl import flags

from google.cloud import storage

flags.DEFINE_string('train_tar', None, 'path for training tar file')
flags.DEFINE_string('val_tar', None, 'path for validation tar file')
flags.DEFINE_string('scratch_dir', None, 'path for scratch directory')
flags.DEFINE_bool('download_extract_only', None, 'Download and extract')
flags.DEFINE_bool('create_tfrecords', None, 'Create records only')

LABELS_FILE = 'synset_labels.txt'

# Imagenet Info : Train - 1281167, Eval - 50000, Classes = 1000

TRAINING_SHARDS = 1024
VALIDATION_SHARDS = 128

TRAINING_DIRECTORY = 'train'
VALIDATION_DIRECTORY = 'validation'

LABELS_URL = 'https://raw.githubusercontent.com/tensorflow/models/master/research/inception/inception/data/imagenet_2012_validation_synset_labels.txt'  # pylint: disable=line-too-long
GCS_BUCKET = 'tpu-gke-bucket'

FLAGS = flags.FLAGS

def check_and_create_dir(directory):
    if not tf.gfile.Exists(directory):
        tf.gfile.MakeDirs(directory)

def download_extract(scratch_dir):
    def download(url,filepath):
        urlretrieve(url,filename=filepath)
    
    def get_all_members(filepath):
        tar = tarfile.open(filepath)
        members = tar.getmembers()
        tar.close()
        return members
    
    def untar_file(filename,member,directory):
        check_and_create_dir(directory)
        tar = tarfile.open(filename)
        if member is not None:
            tar.extract(member,path=directory)
        else:
            tar.extractall(path=directory)

    # Extracting the training data
    tf.logging.info('Extracting training set.')
    members = get_all_members(FLAGS.train_tar)
    directory = os.path.join(scratch_dir,TRAINING_DIRECTORY)

    for member in members:
        subdirectory = os.path.join(directory, member.name.split('.')[0])
        untar_file(FLAGS.train_tar, member, subdirectory)

        sub_tarfile = os.path.join(subdirectory, member.name)
        untar_file(sub_tarfile, None, subdirectory)
        os.remove(sub_tarfile)

    # Download synset_labels for validation set
    tf.logging.info('Downloading the validation labels.')
    download(LABELS_URL, os.path.join(scratch_dir, LABELS_FILE))

    # Extracting the validation data
    tf.logging.info('Extracing the validation set.')
    directory = os.path.join(scratch_dir, VALIDATION_DIRECTORY)
    untar_file(FLAGS.val_tar, None, directory)

def int64_list_feature(value):
    if not isinstance(value,list):
        value = [value]
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))

def bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

# Tensorflow example proto buffer definition
# per image based 
def convert_to_example(filename, image_buffer, label, synset, height, width):

  colorspace = 'RGB'.encode('utf-8')
  channels = 3
  image_format = 'JPEG'.encode('utf-8')
  sysnet_bytes = synset.encode('utf-8')
  filename_bytes = os.path.basename(filename).encode('utf-8')

  example = tf.train.Example(features=tf.train.Features(feature={
      'image/height': int64_list_feature(height),
      'image/width': int64_list_feature(width),
      'image/colorspace': bytes_feature(colorspace),
      'image/channels': int64_list_feature(channels),
      'image/class/label': int64_list_feature(label),
      'image/class/synset': bytes_feature(sysnet_bytes),
      'image/format': bytes_feature(image_format),
      'image/filename': bytes_feature(filename_bytes),
      'image/encoded': bytes_feature(image_buffer)}))
  return example

# ----- Un-necessary code can use OpenCV for reading,conversion -----

def is_png(filename):
  # File list from:
  # https://github.com/cytsai/ilsvrc-cmyk-image-list
  return 'n02105855_2933.JPEG' in os.path.basename(filename)


def is_cmyk(filename):
  # File list from:
  # https://github.com/cytsai/ilsvrc-cmyk-image-list
  blacklist = set(['n01739381_1309.JPEG', 'n02077923_14822.JPEG',
                   'n02447366_23489.JPEG', 'n02492035_15739.JPEG',
                   'n02747177_10752.JPEG', 'n03018349_4028.JPEG',
                   'n03062245_4620.JPEG', 'n03347037_9675.JPEG',
                   'n03467068_12171.JPEG', 'n03529860_11437.JPEG',
                   'n03544143_17228.JPEG', 'n03633091_5218.JPEG',
                   'n03710637_5125.JPEG', 'n03961711_5286.JPEG',
                   'n04033995_2932.JPEG', 'n04258138_17003.JPEG',
                   'n04264628_27969.JPEG', 'n04336792_7448.JPEG',
                   'n04371774_5854.JPEG', 'n04596742_4225.JPEG',
                   'n07583066_647.JPEG', 'n13037406_4650.JPEG'])
  return os.path.basename(filename) in blacklist


class ImageCoder(object):
  def __init__(self):
    # Create a single Session to run all image coding calls.
    self._sess = tf.Session()

    # Initializes function that converts PNG to JPEG data.
    self._png_data = tf.placeholder(dtype=tf.string)
    image = tf.image.decode_png(self._png_data, channels=3)
    self._png_to_jpeg = tf.image.encode_jpeg(image, format='rgb', quality=100)

    # Initializes function that converts CMYK JPEG data to RGB JPEG data.
    self._cmyk_data = tf.placeholder(dtype=tf.string)
    image = tf.image.decode_jpeg(self._cmyk_data, channels=0)
    self._cmyk_to_rgb = tf.image.encode_jpeg(image, format='rgb', quality=100)

    # Initializes function that decodes RGB JPEG data.
    self._decode_jpeg_data = tf.placeholder(dtype=tf.string)
    self._decode_jpeg = tf.image.decode_jpeg(self._decode_jpeg_data, channels=3)

  def png_to_jpeg(self, image_data):
    return self._sess.run(self._png_to_jpeg,
                          feed_dict={self._png_data: image_data})

  def cmyk_to_rgb(self, image_data):
    return self._sess.run(self._cmyk_to_rgb,
                          feed_dict={self._cmyk_data: image_data})

  def decode_jpeg(self, image_data):
    image = self._sess.run(self._decode_jpeg,
                           feed_dict={self._decode_jpeg_data: image_data})
    assert len(image.shape) == 3
    assert image.shape[2] == 3
    return image


def process_image(filename, coder):
  # Read the image file.
  with tf.gfile.FastGFile(filename, 'rb') as f:
    image_data = f.read()

  # Clean the dirty data.
  if is_png(filename):
    # 1 image is a PNG.
    tf.logging.info('Converting PNG to JPEG for %s' % filename)
    image_data = coder.png_to_jpeg(image_data)
  elif is_cmyk(filename):
    # 22 JPEG images are in CMYK colorspace.
    tf.logging.info('Converting CMYK to RGB for %s' % filename)
    image_data = coder.cmyk_to_rgb(image_data)

  # Decode the RGB JPEG.
  image = coder.decode_jpeg(image_data)

  # Check that image converted to RGB
  assert len(image.shape) == 3
  height = image.shape[0]
  width = image.shape[1]
  assert image.shape[2] == 3

  return image_data, height, width

# ----- Un-necessary code can use OpenCV for reading,conversion -----

def process_images_in_shard(coder, output_file, filenames, synsets, labels):
    writer = tf.io.TFRecordWriter(path=output_file)

    for file, sysnet_label in zip(filenames,synsets):
        image_data, height, width = process_image(file,coder)
        label = labels[sysnet_label]
        example = convert_to_example(file,image_data,label,sysnet_label,height,width)
        writer.write(example.SerializeToString())
    
    writer.close()

def process_dataset(filenames,sysnets,labels,output_directory,num_shards,prefix):
    check_and_create_dir(output_directory)
    # Divide in chunks
    chunk_size = int(math.ceil(len(filenames) / num_shards))
    coder = ImageCoder()
    files = []

    for shard in range(num_shards):
        chunk_files = filenames[shard * chunk_size : (shard + 1) * chunk_size]
        chunk_sysnets = sysnets[shard * chunk_size : (shard + 1) * chunk_size]

        if len(chunk_files) < chunk_size:
            tf.logging.info(
                'shard index : {0}, chunk size : {1}'.format(shard,len(chunk_files)))
        
        # TFRecord file
        output_file = os.path.join(
            output_directory, '%s-%.5d-of-%.5d' % (prefix, shard, num_shards))
        process_images_in_shard(coder,output_file,chunk_files,chunk_sysnets,labels)
        tf.logging.info('Finished writing file: %s' % output_file)
        files.append(output_file)

    return files

def create_records(scratch_dir):
    # Shuffle training records to ensure we are distributing classes
    # across the batches.
    random.seed(0)
    def make_shuffle_idx(n):
        order = [i for i in range(n)]
        random.shuffle(order)
        return order

    # Glob all the training files
    training_files = tf.gfile.Glob(
        os.path.join(scratch_dir, TRAINING_DIRECTORY, '*', '*.JPEG'))
    
    # Get training file synset labels from the directory name
    training_synsets = [
        os.path.basename(os.path.dirname(f)) for f in training_files]

    training_shuffle_idx = make_shuffle_idx(len(training_files))
    training_files = [training_files[i] for i in training_shuffle_idx]
    training_synsets = [training_synsets[i] for i in training_shuffle_idx]

    # Glob all the validation files
    validation_files = sorted(tf.gfile.Glob(
        os.path.join(scratch_dir, VALIDATION_DIRECTORY, '*.JPEG')))

    # Get validation file synset labels from labels.txt
    validation_synsets = tf.gfile.FastGFile(
        os.path.join(scratch_dir, LABELS_FILE), 'r').read().splitlines()

    # Create unique ids for all synsets
    labels = {v: k + 1 for k, v in enumerate(
        sorted(set(validation_synsets + training_synsets)))}

    # Create training data
    tf.logging.info('Processing the training data.')

    training_records = process_dataset(
        training_files, training_synsets, labels,
        os.path.join(scratch_dir, TRAINING_DIRECTORY),
        TRAINING_SHARDS, TRAINING_DIRECTORY)

    # Create validation data
    tf.logging.info('Processing the validation data.')
    validation_records = process_dataset(
        validation_files, validation_synsets, labels,
        os.path.join(scratch_dir, VALIDATION_DIRECTORY),
        VALIDATION_SHARDS, VALIDATION_DIRECTORY)

    return training_records, validation_records

def upload_gcs(training_records, validation_records):
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(GCS_BUCKET)

    def upload_files(records, gcs_blob_path):
        for record_path in records:
            record_name = os.path.basename(record_path)
            destination_blob_name = os.path.join(gcs_blob_path, record_name)

            blob = bucket.blob(destination_blob_name)
            blob.upload_from_filename(record_path)

            tf.logging.info('Finished uploading file: %s' % record_name)
    
    train_cloud_path = os.path.join('Imagenet', TRAINING_DIRECTORY)
    upload_files(training_records, train_cloud_path)

    train_cloud_path = os.path.join('Imagenet', VALIDATION_DIRECTORY)
    upload_files(validation_records, train_cloud_path)
    


def main(argv): 
    del argv
    tf.logging.set_verbosity(tf.logging.INFO)
    if FLAGS.download_extract_only:
        if FLAGS.scratch_dir is None:
           tf.logging.info('Setup a proper scratch dir')
           return
        if FLAGS.train_tar is None:
           tf.logging.info('Setup a proper train tar file')
           return
        if FLAGS.val_tar is None:
           tf.logging.info('Setup a proper validation tar file')
           return

        check_and_create_dir(FLAGS.scratch_dir)
        download_extract(FLAGS.scratch_dir)
        return


    if FLAGS.create_tfrecords:
        tf.logging.info('Create records only')
        if not tf.gfile.Exists(FLAGS.scratch_dir):
            tf.logging.info('Extract the data first.')
        
        # Create tf records
        training_records, validation_records = create_records(FLAGS.scratch_dir)
        upload_gcs(training_records, validation_records)

        # Upload to gcs bucket

if __name__ == '__main__':
    app.run(main)