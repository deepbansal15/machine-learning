# PPO implementation
import os
import random
import math
import time

import tensorflow as tf
import numpy as np
from ppo import core

# Always import roboschool before gym 
# because of env conflicts
import roboschool
import gym
from gym.spaces import Box, Discrete

# Do parallel later but finish everything today

# AIM - 1
# On-policy advantage and policy optimization using gradient descent
# AIM - 2 
# Off-policy advantage estimation and policy optimization using gradient descent
# AIM - 3
# Multi process with both on and off-policy advantage estimation
# AIM - 4
# Random network distillation

# Gather on policy experience



EPS = 1e-8

# Implementing batch actor critic buffer for stochastic policy
# Using sum of rewards as value function target
# Can also use r(s,a) + gamma * v(s+1,a)
# Equation implemented 
class PPOBuffer:
    """
    A buffer for storing trajectories experienced by a PPO agent interacting
    with the environment, and using Generalized Advantage Estimation (GAE-Lambda)
    for calculating the advantages of state-action pairs.
    """

    def __init__(self, obs_dim, act_dim, size, gamma=0.99, lam=0.95):
        self.obs_buf = np.zeros(core.combined_shape(size, obs_dim), dtype=np.float32)
        self.act_buf = np.zeros(core.combined_shape(size, act_dim), dtype=np.float32)
        self.adv_buf = np.zeros(size, dtype=np.float32)
        self.rew_buf = np.zeros(size, dtype=np.float32)
        self.ret_buf = np.zeros(size, dtype=np.float32)
        self.val_buf = np.zeros(size, dtype=np.float32)
        self.logp_buf = np.zeros(size, dtype=np.float32)
        self.gamma, self.lam = gamma, lam
        self.ptr, self.path_start_idx, self.max_size = 0, 0, size

    def store(self, obs, act, rew, val, logp):
        """
        Append one timestep of agent-environment interaction to the buffer.
        """
        assert self.ptr < self.max_size     # buffer has to have room so you can store
        self.obs_buf[self.ptr] = obs
        self.act_buf[self.ptr] = act
        self.rew_buf[self.ptr] = rew
        self.val_buf[self.ptr] = val
        self.logp_buf[self.ptr] = logp
        self.ptr += 1

    def finish_path(self, last_val=0):
        """
        Call this at the end of a trajectory, or when one gets cut off
        by an epoch ending. This looks back in the buffer to where the
        trajectory started, and uses rewards and value estimates from
        the whole trajectory to compute advantage estimates with GAE-Lambda,
        as well as compute the rewards-to-go for each state, to use as
        the targets for the value function.

        The "last_val" argument should be 0 if the trajectory ended
        because the agent reached a terminal state (died), and otherwise
        should be V(s_T), the value function estimated for the last state.
        This allows us to bootstrap the reward-to-go calculation to account
        for timesteps beyond the arbitrary episode horizon (or epoch cutoff).
        """

        path_slice = slice(self.path_start_idx, self.ptr)
        rews = np.append(self.rew_buf[path_slice], last_val)
        vals = np.append(self.val_buf[path_slice], last_val)
        
        # the next two lines implement GAE-Lambda advantage calculation
        deltas = rews[:-1] + self.gamma * vals[1:] - vals[:-1]
        self.adv_buf[path_slice] = core.discount_cumsum(deltas, self.gamma * self.lam)
        
        # the next line computes rewards-to-go, to be targets for the value function
        self.ret_buf[path_slice] = core.discount_cumsum(rews, self.gamma)[:-1]
        
        self.path_start_idx = self.ptr

    def get(self):
        """
        Call this at the end of an epoch to get all of the data from
        the buffer, with advantages appropriately normalized (shifted to have
        mean zero and std one). Also, resets some pointers in the buffer.
        """
        print(self.ptr)
        print(self.max_size)
        assert self.ptr == self.max_size    # buffer has to be full before you can get
        self.ptr, self.path_start_idx = 0, 0
        # the next two lines implement the advantage normalization trick
        # adv_mean, adv_std = mpi_statistics_scalar(self.adv_buf)
        # No ampi alternative
        adv_mean = np.mean(self.adv_buf)
        adv_std = np.std(self.adv_buf)
        self.adv_buf = (self.adv_buf - adv_mean) / adv_std
        return [self.obs_buf, self.act_buf, self.adv_buf, 
                self.ret_buf, self.logp_buf]

# Code from openai baseline with tensorflow summaries
def ppo(env_fn, actor_critic=core.actor_critic, seed=0, 
        steps_per_epoch=1000, epochs=50, gamma=0.99, clip_ratio=0.2, pi_lr=3e-4,
        vf_lr=1e-3, train_pi_iters=20, train_v_iters=20, lam=0.97, max_ep_len=1000,
        target_kl=0.01, save_freq=10, summ_dir=None, model_dir=None):

    core.check_and_create_dir(model_dir)
    core.check_and_create_dir(summ_dir)

    tf.set_random_seed(seed)
    np.random.seed(seed)

    action_space, obs_space = env_fn.get_space_dims()
    obs_dim = obs_space.shape
    act_dim = action_space.shape

    x_ph, a_ph = core.placeholders_from_spaces(obs_space, action_space)
    adv_ph, ret_ph, logp_old_ph = core.placeholders(None, None, None)

    pi, logp, logp_pi, v = actor_critic(x=x_ph, a=a_ph,action_space=action_space)

    all_phs = [x_ph, a_ph, adv_ph, ret_ph, logp_old_ph]

    get_action_ops = [pi, v, logp_pi]

    # Will change after putting mpi
    # Not necessary for Unity environment
    local_steps_per_epoch = int(steps_per_epoch)
    buf = PPOBuffer(obs_dim, act_dim, local_steps_per_epoch, gamma, lam)

    var_counts = tuple(core.count_vars(scope) for scope in ['pi', 'v'])
    var_counts = np.sum(var_counts)
    print('Number of parameters : {} '.format(var_counts))
    tf.summary.scalar('Number_of_parameters', var_counts)

    ratio = tf.exp(logp - logp_old_ph)
    min_adv = tf.where(adv_ph>0, (1+clip_ratio)*adv_ph, (1-clip_ratio)*adv_ph)
    pi_loss = -tf.reduce_mean(tf.minimum(ratio * adv_ph, min_adv))
    v_loss = tf.reduce_mean((ret_ph - v)**2)

    # Useful info to keep track of
    approx_kl = tf.reduce_mean(logp_old_ph - logp)      # a sample estimate for KL-divergence, easy to compute
    approx_ent = tf.reduce_mean(-logp)                  # a sample estimate for entropy, also easy to compute
    clipped = tf.logical_or(ratio > (1+clip_ratio), ratio < (1-clip_ratio))
    clipfrac = tf.reduce_mean(tf.cast(clipped, tf.float32))

    train_pi = tf.train.AdamOptimizer(learning_rate=pi_lr).minimize(pi_loss)
    train_v = tf.train.AdamOptimizer(learning_rate=vf_lr).minimize(v_loss)

    merged = tf.summary.merge_all()
    
    sess = tf.Session()
    summary_writer = tf.summary.FileWriter(summ_dir, sess.graph)
    saver = tf.train.Saver()
    sess.run(tf.global_variables_initializer())

    def update():
        inputs = {k:v for k, v in zip(all_phs, buf.get())}
        pi_l_old, v_l_old, ent = sess.run([pi_loss, v_loss, approx_ent], feed_dict=inputs)
        
        # Training
        for i in range(train_pi_iters):
            _, kl = sess.run([train_pi, approx_kl], feed_dict=inputs)
            # kl = mpi_avg(kl)
            if kl > 1.5 * target_kl:
                tf.logging.info('Early stopping at step %d due to reaching max kl.'%i)
                break
        
        tf.summary.scalar('Training_stop_iter',i)
        for _ in range(train_v_iters):
            sess.run(train_v, feed_dict=inputs)
        
        pi_l_new, v_l_new, kl, cf = sess.run([pi_loss, v_loss, approx_kl, clipfrac], feed_dict=inputs)
        
        tf.summary.scalar('Loss_pi', pi_l_old)
        tf.summary.scalar('Loss_v', v_l_old)
        tf.summary.scalar('KL', kl)
        tf.summary.scalar('Entropy', ent)
        tf.summary.scalar('Clip_Fraction_pi', cf)
        tf.summary.scalar('Delta_Loss_pi', (pi_l_new - pi_l_old))
        tf.summary.scalar('Delta_Value_pi', (v_l_new - v_l_old))

    start_time = time.time()
    o, r, d, ep_ret, ep_len = env_fn.reset(), 0, False, 0, 0   

    for epoch in range(epochs):
        for t in range(local_steps_per_epoch):
            a, v_t, logp_t = sess.run(get_action_ops, feed_dict={x_ph: np.expand_dims(o, axis=0)})

            # save and log
            buf.store(o, a, r, v_t, logp_t)
            # tf.summary.scalar('V_Vals',v_t)

            o, r, d, _ = env_fn.step(a[0])
            ep_ret += r
            ep_len += 1

            terminal = d or (ep_len == max_ep_len)
            if terminal or (t==local_steps_per_epoch-1):
                if not(terminal):
                    print('Warning: trajectory cut off by epoch at %d steps.'%ep_len)
                # if trajectory didn't reach terminal state, bootstrap value target
                last_val = r if d else sess.run(v, feed_dict={x_ph: np.expand_dims(o, axis=0)})
                buf.finish_path(last_val)
                if terminal:
                    # only save EpRet / EpLen if trajectory finished
                    print('Ep ret {}, Ep len {}'.format(ep_ret, ep_len))
                    # tf.summary.scalar('Ep_Ret', ep_ret)
                    # tf.summary.scalar('Ep_len', ep_len)
                    
                o, r, d, ep_ret, ep_len = env_fn.reset(), 0, False, 0, 0

        # Perform PPO update!
        update()

        print('Epoch finished {}'.format(epoch))
        tf.summary.scalar('Epoch_time', time.time()-start_time)

        # Save model
        if (epoch % save_freq == 0) or (epoch == epochs-1):
            saver.save(sess, model_dir + '/model.ckpt')
        
        # Save summaries
        summary = sess.run(merged)
        summary_writer.add_summary(summary, epoch)
    
    summary_writer.close()
    sess.close()





