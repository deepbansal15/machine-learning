# PPO core implementation
import tensorflow as tf
import numpy as np
import scipy
from scipy import signal

# Always import roboschool before gym 
# because of env conflicts
import roboschool
import gym
from gym.spaces import Box, Discrete

EPS = 1e-8

def get_all_envs(debug=False):
    from gym import envs
    env_specs = envs.registry.all()
    roboschool_list = [env_spec.id for env_spec in env_specs if env_spec.id.startswith('Roboschool')]
    gym_list = [env_spec.id for env_spec in env_specs if env_spec.id not in roboschool_list]
    if debug:
        print('Roboschool envs: {}'.format(roboschool_list))
        print('Gym envs: {}'.format(gym_list))
    
    return {'roboschool_env':roboschool_list, 'gym_env':gym_list}

def combined_shape(length, shape=None):
    if shape is None:
        return (length,)
    return (length, shape) if np.isscalar(shape) else (length, *shape)

def placeholder(dim=None):
    return tf.placeholder(dtype=tf.float32, shape=combined_shape(None,dim))

def placeholders(*args):
    return [placeholder(dim) for dim in args]

def placeholder_from_space(space):
    if isinstance(space, Box):
        return placeholder(space.shape)
    elif isinstance(space, Discrete):
        return tf.placeholder(dtype=tf.int32, shape=(None,))
    raise NotImplementedError

def placeholders_from_spaces(*args):
    return [placeholder_from_space(space) for space in args]

def get_vars(scope=''):
    return [x for x in tf.trainable_variables() if scope in x.name]

def count_vars(scope=''):
    v = get_vars(scope)
    return sum([np.prod(var.shape.as_list()) for var in v])

def check_and_create_dir(directory):
    if not tf.gfile.Exists(directory):
        tf.gfile.MakeDirs(directory)

def mlp(x,hidden_sizes,activation,output_activation):
    x = tf.layers.conv2d(inputs=x, filters=10, kernel_size=3, padding='same', activation=activation)
    x = tf.layers.max_pooling2d(x, pool_size=(4,4), strides=2)
    x = tf.layers.flatten(x)
    for size in hidden_sizes[:-1]:
        x = tf.layers.dense(x,units=size,activation=activation)
    
    return tf.layers.dense(x, units=hidden_sizes[-1],activation=output_activation)

def gaussian_likelihood(x, mu, log_std):
    pre_sum = -0.5 * (((x-mu)/(tf.exp(log_std)+EPS))**2 + 2*log_std + np.log(2*np.pi))
    return tf.reduce_sum(pre_sum, axis=1)

def gaussian_policy(x, a, hidden_sizes, activation, output_activation, action_space):
    ''' 
    Action(a) as a tensorflow placeholder
    '''
    act_dim = action_space.shape.as_list()[-1]
    mu = mlp(x, list(hidden_sizes)+[act_dim], activation, output_activation)
    log_std = tf.get_variable(name='log_std', 
                    initializer=-0.5*np.ones(act_dim, dtype=np.float32))
    std = tf.exp(log_std)
    # Reparamaterization trick
    pi = mu + tf.random_normal(tf.shape(mu)) * std
    logp = gaussian_likelihood(a, mu, log_std)
    logp_pi = gaussian_likelihood(pi, mu, log_std)
    return pi, logp, logp_pi

def categorical_policy(x, a, hidden_sizes, activation, output_activation, action_space):
    act_dim = action_space.n
    logits = mlp(x, list(hidden_sizes)+[act_dim], activation, None)
    logp_all = tf.nn.log_softmax(logits)
    pi = tf.squeeze(tf.multinomial(logits,1), axis=1)
    logp = tf.reduce_sum(tf.one_hot(a, depth=act_dim) * logp_all, axis=1)
    logp_pi = tf.reduce_sum(tf.one_hot(pi, depth=act_dim) * logp_all, axis=1)
    return pi, logp, logp_pi

# Code from OpenAI
def discount_cumsum(x, discount):
    """
    magic from rllab for computing discounted cumulative sums of vectors.

    input: 
        vector x, 
        [x0, 
         x1, 
         x2]

    output:
        [x0 + discount * x1 + discount^2 * x2,  
         x1 + discount * x2,
         x2]
    """
    return scipy.signal.lfilter([1], [1, float(-discount)], x[::-1], axis=0)[::-1]

def actor_critic(x, a, hidden_sizes=(64,64), activation=tf.tanh, 
        output_activation=None, policy=None, action_space=None
    ):

    # default policy builder depends on action space
    if policy is None and isinstance(action_space, Box):
        policy = gaussian_policy
    elif policy is None and isinstance(action_space, Discrete):
        policy = categorical_policy

    with tf.variable_scope('pi'):
        pi, logp, logp_pi = policy(x, a, hidden_sizes, activation, output_activation, action_space)
    with tf.variable_scope('v'):
        v = tf.squeeze(mlp(x, list(hidden_sizes)+[1], activation, None), axis=1)
    return pi, logp, logp_pi, v