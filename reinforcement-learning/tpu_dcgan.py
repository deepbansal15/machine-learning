# Use keras model
# Keep data on gcs bucket
# train using code experiments from tpu_test
# keras model save (fetch weights from tpu, \
# load on a non-replicated model and save on local disk)
# will be compatible with CPU and GPU models

# Keras model with tf.nn model in reinforcement learning
# Can also create model in class with keras

# Use keras_to_tpu to transfer the model instead of strategy (Not sure if it will compile it as well)
# TOTRY : create a keras model inside strategy and outside and save and load weights interchangeably
# keras_to_tpu is gone directly under strategy without compile
import os
import time

import tensorflow as tf
from tensorflow.python.keras.utils import losses_utils
# from tensorflow.keras import losses.Reduction
import numpy as np

from absl import app
from absl import flags

tfk = tf.keras
tfl = tfk.layers

# output_shape = (224,224,3)
def generator_model(input_shape):
    return tfk.Sequential([
        tfl.Dense(7*7*256, use_bias=False, input_shape=input_shape),
        tfl.BatchNormalization(),
        tfl.LeakyReLU(),
        tfl.Reshape((7,7,256)),
        tfl.Conv2DTranspose(128, 5, strides=(1,1),
                            padding='same', use_bias=False),
        # Doesn't work with TPU because of if statement indside tfl.BatchNormalization(),
        tfl.LeakyReLU(),
        tfl.Conv2DTranspose(64, 5, strides=(2,2),
                                padding='same', use_bias=False),
        # tfl.BatchNormalization(),
        tfl.LeakyReLU(),
        tfl.Conv2DTranspose(32, 5, strides=(2,2),
                                padding='same', use_bias=False),
        # tfl.BatchNormalization(),
        tfl.LeakyReLU(),
        tfl.Conv2DTranspose(16, 5, strides=(2,2),
                                padding='same', use_bias=False),
        # tfl.BatchNormalization(),
        tfl.LeakyReLU(),
        tfl.Conv2DTranspose(8, 5, strides=(2,2),
                                padding='same', use_bias=False),
        # tfl.BatchNormalization(),
        tfl.LeakyReLU(),
        tfl.Conv2DTranspose(3, 5, strides=(2,2),
                                padding='same', use_bias=False),
    ])

def discriminator_model(input_shape):
    return tfk.Sequential([
        tfl.Conv2D(64,5,strides=(2,2), padding='same', input_shape=input_shape),
        tfl.LeakyReLU(),
        # tfl.Dropout(0.3) doesn't work because of if statement
        # tfl.Dropout(0.3),
        tfl.Conv2D(128, 5, strides=(2,2), padding='same'),
        tfl.LeakyReLU(),
        # tfl.Dropout(0.3),
        tfl.Flatten(),
        tfl.Dense(1)
    ])

class DCGan():
    def __init__(self):
        gen_input_shape = (100,)

        tpu_model = tf.contrib.tpu.keras_to_tpu_model(model, strategy=strategy)
        
        self.generator = generator_model(gen_input_shape)
        # dis_input_shape = self.generator.compute_output_shape((None,100))[1:]
        dis_input_shape = (224, 224, 3)
        print('discriminator input shape {}'.format(dis_input_shape))
        self.discriminator = discriminator_model(dis_input_shape)
        # Bug in tensorflow api mentioned in the documentation doesn't exists
        self.bce = tfk.losses.BinaryCrossentropy(from_logits=True, reduction='none')
        gen_optimizer = tf.train.AdamOptimizer(learning_rate=1e-4)
        disc_optimizer = tf.train.AdamOptimizer(learning_rate=1e-4)
        self.discriminator_optimizer = tf.contrib.tpu.CrossShardOptimizer(disc_optimizer)
        self.generator_optimizer = tf.contrib.tpu.CrossShardOptimizer(gen_optimizer)
        self.checkpoint = tf.train.Checkpoint(
            gen_optimizer = self.generator_optimizer,
            dis_optimizer = self.discriminator_optimizer,
            generator = self.generator,
            discriminator = self.discriminator)
    
    def generator_loss(self, generated_output):
        return self.bce(tf.ones_like(generated_output), generated_output)
    
    def discriminator_loss(self, real, generated):
        real_loss = self.bce(tf.ones_like(real), real)
        generated_loss = self.bce(tf.zeros_like(generated), generated)
        return real_loss + generated_loss
    
    def train_step(self, dist_inputs, strategy, global_batch_size):
        def step_fn(inputs):
            image, noise = inputs
            # with tf.GradientTape() as dis_tape, tf.GradientTape() as gen_tape:
            generated_image = self.generator(noise)

            real_output = self.discriminator(image)
            generated_output = self.discriminator(generated_image)

            gen_loss = self.generator_loss(generated_output)
            dis_loss = self.discriminator_loss(real_output, generated_output)
            
            gen_loss = tf.reduce_sum(gen_loss) * (1.0 / global_batch_size)
            dis_loss = tf.reduce_sum(dis_loss) * (1.0 / global_batch_size)

            # gen_grads = gen_tape.gradient(gen_loss, self.generator.trainable_variables)
            # dis_grads = dis_tape.gradient(dis_loss, self.discriminator.trainable_variables)
            # self.generator_optimizer.apply_gradients(zip(gen_grads, self.generator.trainable_variables))
            # self.discriminator_optimizer.apply_gradients(zip(dis_grads, self.discriminator.trainable_variables))
            # Return a mean of losses from all the cores
            gen_train_op = self.generator_optimizer.minimize(loss=gen_loss, var_list=self.generator.trainable_variables)
            dis_train_op = self.discriminator_optimizer.minimize(loss=dis_loss, var_list=self.discriminator.trainable_variables)
            
            # update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            with tf.control_dependencies([gen_train_op, dis_train_op]):
                return [tf.identity(gen_loss), tf.identity(dis_loss)]
            
        per_replica_losses = strategy.experimental_run_v2(
            step_fn, args=(dist_inputs,))
        gen_mean_loss = strategy.reduce(
            tf.distribute.ReduceOp.SUM, per_replica_losses[0], axis=None)
        dis_mean_loss = strategy.reduce(
            tf.distribute.ReduceOp.SUM, per_replica_losses[1], axis=None)
        return [gen_mean_loss, dis_mean_loss]

def setup_and_train_model():
    cluster_resolver = tf.distribute.cluster_resolver.TPUClusterResolver(
        tpu='tpu-vm-v3-1', project='reinforcement-learning-244916'
    )
    topology = tf.tpu.experimental.initialize_tpu_system(cluster_resolver)
    tpu_strategy = tf.distribute.experimental.TPUStrategy(
        tpu_cluster_resolver=cluster_resolver,
        steps_per_run=1
    )

    BATCH_SIZE_PER_REPLICA = 8
    global_batch_size = (BATCH_SIZE_PER_REPLICA *
        tpu_strategy.num_replicas_in_sync)


    np_images_input = np.ones((512,224,224,3),dtype=np.float)
    np_noise_input = np.random.normal(size=(512,100))
    with tpu_strategy.scope():
        image_placeholder = tf.placeholder(dtype=tf.float32, shape=(None,224,224,3))
        noise_placeholder = tf.placeholder(dtype=tf.float32, shape=(None,100))
        gan = DCGan()

        dataset = tf.data.Dataset.from_tensor_slices((image_placeholder, noise_placeholder)).batch(global_batch_size)
        dataset = dataset.repeat()
        dist_dataset = tpu_strategy.experimental_distribute_dataset(dataset)
    
    with tpu_strategy.scope():
        input_iterator = dist_dataset.make_initializable_iterator()
        iterator_init = input_iterator.initialize()
        var_init = tf.global_variables_initializer()
        loss = gan.train_step(input_iterator.get_next(), tpu_strategy, global_batch_size)

        config = tf.ConfigProto()
        config.log_device_placement = True
        # config.allow_soft_placement = True
        cluster_spec = cluster_resolver.cluster_spec()
        print('cluster spec {}'.format(cluster_spec))
        if cluster_spec:
            config.cluster_def.CopyFrom(cluster_spec.as_cluster_def())
        with tf.Session(target=cluster_resolver.master(), config=config) as sess:
            sess.run([var_init, iterator_init], feed_dict={image_placeholder:np_images_input, noise_placeholder:np_noise_input})
            start_time = time.time()
            for _ in range(100):
                try:
                    print(sess.run(loss))
                except tf.errors.OutOfRangeError:
                    print('Out of range exception')
                    break
            end_time = time.time()
            print('time taken {} '.format(end_time - start_time))
            # print(model.summary)
            # weights = model.get_weights()
            # print(weights)
            # save_path = '/home/deepbansal/machine-learning/reinforcement-learning/keras_tpu_model'
            # if not tf.io.gfile.exists(save_path):
                # tf.io.gfile.makedirs(save_path)
            # # tf.train.Saver().save(sess,save_path + '/model')
            # model.save(filepath=save_path + '/model')
            # tf.keras.models.save_model(model,save_path + '/model','tf')

def main(argv):
    del(argv)
    setup_and_train_model()

if __name__ == '__main__':
    app.run(main)


