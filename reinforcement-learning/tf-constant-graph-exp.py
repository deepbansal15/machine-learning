import tensorflow as tf
from absl import app
from absl import flags

flags.DEFINE_integer('int_val',-1,'int value')
flags.DEFINE_string('str_val',None,'string value')
flags.DEFINE_integer('batch_size',16,'batch size')

# if validator is true then its acceptable else not
# flags.register_multi_flags_validator(
#     ['int_val','str_val'],
#     lambda flags: (flags['int_val'] != -1 and flags['str_val'] != None),
#     'Flags need to be initialized'
# )

FLAGS = flags.FLAGS

height = 32
width = 32
total_examples = 100

def predict_input_fn(params):
  batch_size = params['batch_size']

  images = tf.random.uniform(
      [total_examples, height, width, 3], minval=-1, maxval=1)

  dataset = tf.data.Dataset.from_tensor_slices(images)

  # shape without map (?,width,height,3)
  # shape with map {'image': Tensorshape([None,widht,height,3])}
  dataset = dataset.map(lambda images: {'image': images})

  dataset = dataset.batch(batch_size)
  return dataset

def model_fn(input):
    x = tf.layers.dense(inputs=input,units=1024,activation=tf.nn.relu)
    x = tf.layers.dense(inputs=x,units=1)
    return x

def custom_getter(getter, name, *args, **kwargs):
    print(name)
    with tf.control_dependencies(None):
        return tf.guarantee_const(
            getter(name, *args, **kwargs), name=name + "/GuaranteeConst")

def constant_model_fn(input):
    with tf.variable_scope('',custom_getter=custom_getter):
        return model_fn(input)

def main(argv):
    del(argv)
    params = FLAGS.flag_values_dict()
    dataset = predict_input_fn(params)
    # session = tf.Session()
    print(dataset.output_shapes)

    sess = tf.Session(graph=tf.Graph())
    with sess.graph.as_default():
        input = tf.get_variable(
            name='input',shape=[1,100],
            dtype=tf.float32,
            initializer=tf.initializers.random_normal
        )

        const_fn = constant_model_fn(input)
        fn = model_fn(input)
        global_ops = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
        sess.run(tf.global_variables_initializer())
        print(global_ops)
    
    sess.close()

    # session.run([dataset])
    # print(params['int_val'])
    # print(params['str_val'])

if __name__ == '__main__':
    app.run(main)


