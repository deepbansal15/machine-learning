import tensorflow as tf
import numpy as np

import time

from absl import app
import densenet_model

import pprint

def create_model(inputs):
    return tf.layers.conv2d(inputs=inputs, kernel_size=(5,5), filters=10)
    # tf.layers.conv2d()
    # x = tf.keras.layers.Conv2D(filters=28, kernel_size=(3, 3), activation='relu', input_shape=input_shape)
    # tf.keras.layers.MaxPooling2D(pool_size=(2, 2))
    # tf.keras.layers.Flatten()
    # tf.keras.layers.Dense(128, activation=tf.nn.relu)
    # tf.keras.layers.Dropout(0.2)
    # tf.keras.layers.Dense(10, activation=tf.nn.softmax)

def add_op(x, y):
  a = x * y + x
  b = a * x
  c = b * y
  return c * b * a

def tpu_add_op(x,y):
    def custom_getter(getter, name, *args, **kwargs):
        with tf.control_dependencies(None):
            return tf.guarantee_const(
                getter(name, *args, **kwargs), name=name + "/GuaranteeConst")
    with tf.variable_scope("", custom_getter=custom_getter):
        return add_op(x,y)

def cpu_run(op, in_ph, value):
    
    
    sess = tf.Session()
    # ph = tf.placeholder(dtype=tf.float32, shape=(None,225,225,3))
    # op = add_op(ph, ph)

    for _ in range(20):
        sess.run(op,feed_dict={in_ph:value})

    sess.close()

def create_dataset():
    np_input = np.ones((1000,225,225,3))


def tpu_run_with_strategy(in_ph):
    np_input = np.ones((1000,225,225,3))

    cluster_resolver = tf.distribute.cluster_resolver.TPUClusterResolver(
        tpu='tpu-vm-v3-1', project='reinforcement-learning-244916'
    )
    topology = tf.tpu.experimental.initialize_tpu_system(cluster_resolver)
    tpu_strategy = tf.distribute.experimental.TPUStrategy(
        tpu_cluster_resolver=cluster_resolver,
        steps_per_run=1
    )

    with tpu_strategy.scope():
        # tpu_op = add_op(in_ph, np_input)
        logits = densenet_model.densenet_imagenet_121(in_ph)
        all_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
        # pprint.pprint(all_vars)
        # logits = tf.tpu.batch_parallel(logits, inputs=[in_ph], num_shards=8)
        # tpu_op_2 = tf.tpu.batch_parallel(logits, inputs=[in_ph, in_ph], num_shards=8)
    
    config = tf.ConfigProto()
    config.log_device_placement = True
    # config.allow_soft_placement = True
    cluster_spec = cluster_resolver.cluster_spec()
    if cluster_spec:
        config.cluster_def.CopyFrom(cluster_spec.as_cluster_def())

    with tf.Session(target=cluster_resolver.master(), config=config) as session:
        session.run(tf.global_variables_initializer())
        start_time = time.time()
        # for _ in range(20):
        output = session.run(logits,feed_dict={in_ph:np_input})
        # output = session.run(tpu_op_2,feed_dict={in_ph:np_input})
        print('time_taken {}'.format(time.time() - start_time))
    pass

def tpu_run(in_ph, np_input):
    # tpu_op_1 = tf.tpu.rewrite(tpu_add_op, inputs=[in_ph,in_ph])
    tpu_op_2 = tf.tpu.batch_parallel(tpu_add_op, inputs=[in_ph, in_ph], num_shards=8)

    sess = tf.Session('grpc://10.240.1.2:8470')
    sess.run(tf.tpu.initialize_system())
    # devices = sess.list_devices()
    # print(devices)
    for _ in range(20):
        sess.run(tpu_op_2,feed_dict={in_ph:np_input})

    sess.run(tf.tpu.shutdown_system())
    sess.close()


def custom_tpu_keras():

    cluster_resolver = tf.distribute.cluster_resolver.TPUClusterResolver(
        tpu='tpu-vm-v3-1', project='reinforcement-learning-244916'
    )
    topology = tf.tpu.experimental.initialize_tpu_system(cluster_resolver)
    tpu_strategy = tf.distribute.experimental.TPUStrategy(
        tpu_cluster_resolver=cluster_resolver,
        steps_per_run=1
    )

    BATCH_SIZE_PER_REPLICA = 25
    global_batch_size = (BATCH_SIZE_PER_REPLICA *
        tpu_strategy.num_replicas_in_sync)

    def keras_sequential_model():
        return tf.keras.Sequential([
            tf.keras.layers.Conv2D(28, kernel_size=(3, 3), activation='relu', input_shape=(225,225,3)),
            tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
            tf.keras.layers.Conv2D(28, kernel_size=(3, 3), activation='relu'),
            tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(1, activation=tf.nn.softmax)
        ])
    
    def keras_sequential_model2():
        return tf.keras.Sequential([
            tf.keras.layers.Conv2D(28, kernel_size=(3, 3), activation='relu', input_shape=(225,225,3)),
            tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
            tf.keras.layers.Conv2D(28, kernel_size=(3, 3), activation='relu'),
            tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
            tf.keras.layers.Conv2D(28, kernel_size=(2, 2), activation='relu'),
            tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(1, activation=tf.nn.softmax)
        ])
    
    def layer_model(in_val):
        x = tf.layers.conv2d(inputs=in_val, filters=28,
            kernel_size=3, activation='relu',use_bias=False
        )
        x = tf.layers.max_pooling2d(x, pool_size=(2,2), strides=2)
        x = tf.layers.flatten(x)
        x = tf.layers.dense(x,1)
        return x

        

    def train_step(dist_inputs):
        def step_fn(inputs):
            features, labels = inputs
            logits = model(features)
            logits2 = model2(features)
            # logits = layer_model(features)
            print('features shape {} labels hape {} logits shape {}'.format(
                features.get_shape(),labels.get_shape(), logits.get_shape()))
            cross_entropy = tf.nn.softmax_cross_entropy_with_logits(
              logits=logits, labels=labels)
            loss = tf.reduce_sum(cross_entropy) * (1.0 / global_batch_size)
            train_op = optimizer.minimize(loss)
            with tf.control_dependencies([train_op]):
                return tf.identity(loss)

        print('dist_inputs {}'.format(dist_inputs))
        per_replica_losses = tpu_strategy.experimental_run_v2(
            step_fn, args=(dist_inputs,)
        )
        print('per_replica_losses {}'.format(per_replica_losses))
        mean_loss = tpu_strategy.reduce(
            tf.distribute.ReduceOp.SUM, per_replica_losses, axis=None
        )
        return mean_loss

    # (x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
    # x_train, x_test = x_train / 255.0, x_test / 255.0
    np_input = np.ones((1000,225,225,3),dtype=np.float)
    np_labels = np.random.randint(low=0,high=2,size=(1000,1))
    with tpu_strategy.scope():
        feature_placeholder = tf.placeholder(dtype=tf.float32, shape=(None,225,225,3))
        label_placeholder = tf.placeholder(dtype=tf.float32, shape=(None,1))
        
        model = keras_sequential_model()
        model2 = keras_sequential_model2()
        variables = model.trainable_variables
        print('Trainable varibale {}'.format(variables))
        
        optimizer = tf.train.GradientDescentOptimizer(0.1)

        # Requires a session which is eqivalent to tensor slices with placeholders and then session.run with numpy arrays
        # dataset = tpu_strategy.experimental_make_numpy_dataset((np_input, np_labels)).batch(global_batch_size)
        dataset = tf.data.Dataset.from_tensor_slices((feature_placeholder, label_placeholder)).batch(global_batch_size)
        dataset = dataset.repeat()
        # dataset = tf.data.Dataset.from_tensors(([1.], [1.])).repeat(1000).batch(
        #     global_batch_size
        # )
        dist_dataset = tpu_strategy.experimental_distribute_dataset(dataset)
        print('steps per run {} '.format(tpu_strategy.extended.steps_per_run))
    
    with tpu_strategy.scope():
        input_iterator = dist_dataset.make_initializable_iterator()
        iterator_init = input_iterator.initialize()
        var_init = tf.global_variables_initializer()
        loss = train_step(input_iterator.get_next())

        config = tf.ConfigProto()
        config.log_device_placement = True
        # config.allow_soft_placement = True
        cluster_spec = cluster_resolver.cluster_spec()
        print('cluster spec {}'.format(cluster_spec))
        if cluster_spec:
            config.cluster_def.CopyFrom(cluster_spec.as_cluster_def())
        with tf.Session(target=cluster_resolver.master(), config=config) as sess:
            sess.run([var_init, iterator_init], feed_dict={feature_placeholder:np_input, label_placeholder:np_labels})
            start_time = time.time()
            for _ in range(100):
                try:
                    print(sess.run(loss))
                except tf.errors.OutOfRangeError:
                    print('Out of range exception')
                    break
            end_time = time.time()
            print('time taken {} '.format(end_time - start_time))
            print(model.summary)
            weights = model.get_weights()
            # print(weights)
            save_path = '/home/deepbansal/machine-learning/reinforcement-learning/keras_tpu_model'
            if not tf.io.gfile.exists(save_path):
                tf.io.gfile.makedirs(save_path)
            # # tf.train.Saver().save(sess,save_path + '/model')
            model.save(filepath=save_path + '/model')
            # tf.keras.models.save_model(model,save_path + '/model','tf')


    pass


def distribute_dataset_exp():
    cluster_resolver = tf.distribute.cluster_resolver.TPUClusterResolver(
        tpu='tpu-vm-v3-1', project='reinforcement-learning-244916'
    )
    topology = tf.tpu.experimental.initialize_tpu_system(cluster_resolver)
    tpu_strategy = tf.distribute.experimental.TPUStrategy(
        tpu_cluster_resolver=cluster_resolver,
        steps_per_run=1
    )

    # Dataset iterator
    np_input = np.ones((64,225,225,3),dtype=np.float)
    feature_placeholder = tf.placeholder(dtype=tf.float32, shape=(None,225,225,3))

    # Create op
    with tpu_strategy.scope():
        # tpu_op = add_op(in_ph, np_input)
        # logits = densenet_model.densenet_imagenet_121(feature_placeholder)
        # all_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
        model = tf.keras.Sequential([tf.keras.layers.Dense(1, input_shape=(225, 225, 3))])

        def temp_op(in_val):
            x = in_val
            out_val = model(x)
            return out_val
            # x = inputs
            # return tf.cast(x + x,tf.float32)

        config = tf.ConfigProto()
        config.log_device_placement = True
        # config.allow_soft_placement = True
        cluster_spec = cluster_resolver.cluster_spec()
        if cluster_spec:
            config.cluster_def.CopyFrom(cluster_spec.as_cluster_def())

        with tf.Session(target=cluster_resolver.master(), config=config) as session:
            
            # dataset = tpu_strategy.experimental_make_numpy_dataset(np_input, session)
            # dataset = tf.data.Dataset.from_tensor_slices((feature_placeholder))
            # dataset = tf.data.Dataset.from_tensors(([1.])).repeat(1000)
            dataset = tf.data.Dataset.from_tensor_slices(np_input)
            dataset = dataset.batch(64, drop_remainder=True)
            print(dataset.output_shapes)

            # Divide the global batch size equally among all the shards
            dataset = tpu_strategy.experimental_distribute_dataset(dataset)
            iterator = tpu_strategy.make_dataset_iterator(dataset)
            iterator_init = iterator.initialize()

            print(dataset.output_types)

            session.run(tf.global_variables_initializer())
            session.run(iterator_init)
            # session.run(iterator_init, feed_dict={feature_placeholder:np_input})
            dist_op = tpu_strategy.experimental_run(temp_op, iterator).values
            output = session.run(dist_op)
            # print(output)
    pass

def dataset_exp():
    np_input = np.ones((1000,225,225,3))
    feature_placeholder = tf.placeholder(dtype=tf.float32, shape=(None,225,225,3))
    dataset = tf.data.Dataset.from_tensor_slices((feature_placeholder)).batch(8, drop_remainder=True)
    iterator = dataset.make_initializable_iterator()
    next_element = iterator.get_next()
    with tf.Session() as sess:
        sess.run(iterator.initializer, feed_dict={feature_placeholder:np_input})
        while True:
            try:
                value = sess.run(next_element)
            except tf.errors.OutOfRangeError:
                print('Out of range exception')
                break
        
        sess.run(iterator.initializer, feed_dict={feature_placeholder:np_input})
        while True:
            try:
                value = sess.run(next_element)
            except tf.errors.OutOfRangeError:
                print('Out of range exception')
                break
        
        
    pass

def tpu_estimator_model_load():
    variables = tf.train.list_variables('/home/deepbansal/densetnet_model/')
    print(variables)
    pass

def main(argv):
    del(argv)
    tf.logging.set_verbosity(tf.logging.INFO)
    # distribute_dataset_exp()
    custom_tpu_keras()
    return
    
    in_ph = tf.placeholder(dtype=tf.float32, shape=(None,225,225,3))

    # start_time = time.time()
    # cpu_run(in_ph, np_input)
    # cpu_run_time = time.time() - start_time

    # start_time = time.time()
    # tpu_run(in_ph, np_input)
    # tpu_run_time = time.time() - start_time
    # cpu_run(op, in_ph, np_input[:])
    
    start_time = time.time()
    # tpu_run_with_strategy(in_ph)
    tpu_run_with_strategy(in_ph)
    tpu_run_time = time.time() - start_time

    # cpu_run(op, in_ph, np_input[:])

    # print('tpu time: {}, cpu time: {}'.format(tpu_run_time,cpu_run_time))


if __name__ == '__main__':
    app.run(main)