from absl import flags
from absl import app

from ppo.ppo import ppo as PPO
from envs import env_wrapper

flags.DEFINE_string('env', 'SpaceInvaders-v0', 'Gym environment name')
flags.DEFINE_integer('epoch', None, 'Number of epoch to train the policy for')
flags.DEFINE_integer('steps_per_epoch', None, 'No of steps (experience) to train on for 1 epoch')
flags.DEFINE_integer('num_proc', None, 'Number of parallel simulations to run')
flags.DEFINE_string('summ_dir', '/home/deepbansal/machine-learning/work_dir/summ_dir', 'Summary directory')
flags.DEFINE_string('model_dir', '/home/deepbansal/machine-learning/work_dir/model_dir', 'Model directory')

FLAGS = flags.FLAGS

def main(argv):
    del(argv)
    env = env_wrapper.CombinedEnv(FLAGS.env)
    PPO(env_fn=env, summ_dir=FLAGS.summ_dir, model_dir=FLAGS.model_dir)
    env.close()
    pass

if __name__ == '__main__':
    app.run(main)