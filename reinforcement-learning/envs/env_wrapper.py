# provides a single interface for gym and unity

import roboschool
import gym

def get_all_envs(debug=False):
    from gym import envs
    env_specs = envs.registry.all()
    roboschool_list = [env_spec.id for env_spec in env_specs if env_spec.id.startswith('Roboschool')]
    gym_list = [env_spec.id for env_spec in env_specs if env_spec.id not in roboschool_list]
    if debug:
        print('Roboschool envs: {}'.format(roboschool_list))
        print('Gym envs: {}'.format(gym_list))
    
    return {'roboschool_env':roboschool_list, 'gym_env':gym_list}

class CombinedEnv:
    def __init__(self, env_name=None):
        self.env_name = env_name
        self.env = gym.make(self.env_name)
    
    def get_space_dims(self):
        return self.env.action_space, self.env.observation_space

    def step(self, action):
        return self.env.step(action)
    
    def reset(self):
        return self.env.reset()

    def close(self):
        self.env.close()

