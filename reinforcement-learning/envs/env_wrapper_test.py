from absl import flags
from absl import app

import env_wrapper
from env_wrapper import CombinedEnv

flags.DEFINE_string('env', None, 'Environment name')

FLAGS = flags.FLAGS

def test_env(env_name=None):
    env_wrapper.get_all_envs(True)
    env = CombinedEnv(env_name)
    action_space, obs_space = env.get_space_dims()

    print(action_space, obs_space)
    obs = env.reset()
    for i in range(100):
        action = action_space.sample()
        obs, rew, done, info = env.step(action)
        if done:
            obs = env.reset()

def main(argv):
    del(argv)
    test_env(FLAGS.env)

if __name__ == '__main__':
    app.run(main)